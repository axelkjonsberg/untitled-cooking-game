package com.wok_in_progress

import android.content.Context
import android.content.Intent
import android.graphics.Canvas
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.wok_in_progress.entities.EntityManager
import com.wok_in_progress.interaction.InteractionManager
import com.wok_in_progress.physics.PhysicsEngine
import com.wok_in_progress.rendering.FpsCounter
import com.wok_in_progress.rendering.Renderer
import com.wok_in_progress.rules.Game
import com.wok_in_progress.rules.GameManager
import com.wok_in_progress.rules.GameState
import com.wok_in_progress.rules.RuleManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.system.measureNanoTime
import kotlin.system.measureTimeMillis

class GameActivity : AppCompatActivity() {
    private var gameView: GameView? = null
    private val debug = true
    private val performanceTest = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        log("GameActivity: onCreate ran")
        gameView = GameView(this, performanceTest)
        setContentView(gameView)
        PhysicsEngine.debug = debug
        EntityManager.debug = debug
        Renderer.debug = debug
        Game.subscribe(GameState.Quit, ::quit)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        val width = gameView!!.width.toFloat()
        val height = gameView!!.height.toFloat()
        if (Game.gameState == GameState.Unset) {
            GameManager.initializeGame(width, height, this)
        }
        log("GameActivity: onWindowFocusChanged ran, hasFocus: $hasFocus")
    }

    override fun onResume() {
        super.onResume()
        log("GameActivity: onResume triggered")
        Game.resume()
    }

    override fun onPause() {
        super.onPause()
        log("GameActivity: onPause triggered")
        Game.pause()
    }

    private fun quit() {
        log("GameActivity: Ended game")

        log("Score: ${RuleManager.totalScore}")
        val intent = Intent(this, ScoreboardActivity::class.java).apply {
            putExtra("SCORE", RuleManager.totalScore)
        }
        startActivity(intent)
    }

    open class GameView(context: Context) : View(context) {
        //private val renderer = Renderer
        private var performanceTest = false

        constructor(context: Context, performanceTest: Boolean) : this(context) {
            this.performanceTest = performanceTest
        }

        override fun onTouchEvent(event: MotionEvent?): Boolean {
            if (event != null) {
                InteractionManager.handleTouch(event)
            }
            return true
        }

        var timesRan = 0
        override fun onDraw(canvas: Canvas?) {
            CoroutineScope(Dispatchers.Unconfined).launch {
                if (performanceTest) {
                    val timeToRender = measureNanoTime {
                        Renderer.drawCall(canvas!!, context)
                    }
                    if (timesRan % (60*5) == 0) log("Render time: ${timeToRender / 1_000_000f} ms")
                    timesRan++
                    FpsCounter.incrementCounter()
                }
                else {
                    Renderer.drawCall(canvas!!, context)
                }
                invalidate()
                super.onDraw(canvas)
            }
        }
    }
}
