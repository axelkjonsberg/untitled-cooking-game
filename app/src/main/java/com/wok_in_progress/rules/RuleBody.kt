package com.wok_in_progress.rules

class RuleBody(
    var name: String,
    var isDirty: Boolean = false,
    var isPrepared: Boolean = false,
    var isCut: Boolean = false,
    var cooked: Float = 0f // Number from 0-3
) {
    constructor(other: RuleBody) : this(
        name = other.name,
        isDirty = other.isDirty,
        isPrepared = other.isPrepared,
        isCut = other.isCut,
        cooked = other.cooked
    )

    fun cut() {
        if (!isCut) isCut = true
    }

    fun cook() {
        if (cooked < 3) cooked += 0.009f
    }

    fun makeDirty() {
        if (!isDirty) isDirty = true
    }

    fun clean() {
        if (isDirty) isDirty = false
    }
}