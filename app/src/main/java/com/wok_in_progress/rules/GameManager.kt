package com.wok_in_progress.rules

import android.content.Context
import android.graphics.Paint
import android.os.Handler
import com.wok_in_progress.entities.EntityCreator
import com.wok_in_progress.entities.EntityManager
import com.wok_in_progress.interaction.InteractionManager
import com.wok_in_progress.log
import com.wok_in_progress.physics.PhysicsRunnable
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.GameTextures
import com.wok_in_progress.rendering.relX
import com.wok_in_progress.rendering.relY
import com.wok_in_progress.setWorldDimensions

object GameManager {
    var physicsRunnable: PhysicsRunnable? = null
    var timerRunnable: TimerRunnable? = null
    private val gameHandler = Handler()
    private var context: Context? = null

    fun initializeGame(
        width: Float,
        height: Float,
        context: Context
    ) {
        if (Game.gameState != GameState.Unset) return
        physicsRunnable = physicsRunnable ?: PhysicsRunnable(handler = gameHandler)
        timerRunnable = timerRunnable ?: TimerRunnable(handler = gameHandler)
        this.context = context
        Timer.reset()
        RuleManager.totalScore = 0
        setWorldDimensions(width, height)
        GameTextures.loadTextures(context)
        EntityManager.unregisterAll()
        InteractionManager.handleUnselectSection()
        Game.subscribe(GameState.Ended, ::endGame)
        Game.subscribe(GameState.Started, ::startGame)
        Game.subscribe(GameState.StartedRound, ::startRound)
        countDownToStart()
    }

    private fun countDownToStart() {
        var seconds = 4
        val recipeText = EntityCreator.createText(
            "Make this: ${RuleManager.currentDish.key}",
            position = Vector2D(relX(4f / 8f), relY(3f / 8f)),
            align = Paint.Align.CENTER
        )
        val countdownText = EntityCreator.createText(
            "Round starts in $seconds",
            position = Vector2D(relX(4f / 8f), relY(4f / 8f)),
            align = Paint.Align.CENTER
        )
        gameHandler.post(object : Runnable {
            override fun run() {
                if (seconds > 0) gameHandler.postDelayed(this, 1000)
                seconds -= 1
                countdownText.setTextValue("Round starts in $seconds")
                if (seconds == 0) {
                    log("RuleManager: Starting round")
                    EntityManager.unregisterEntity(countdownText)
                    EntityManager.unregisterEntity(recipeText)
                    Game.start()
                }
            }
        })
    }

    private fun startGame() {
        log("RuleManager: Starting game")
        EntityCreator.createGameLayout(context!!)
        Game.run()
    }

    fun endRound() {
        log("RuleManager: Ended round")
        val score = RuleManager.scoreDish()
        EntityManager.unregisterAll()
        InteractionManager.handleUnselectSection()
        val scoreText = EntityCreator.createText(
            "",
            position = Vector2D(relX(1f / 2f), relY(1f / 2f)),
            align = Paint.Align.CENTER
        )
        scoreText.setTextValue("Score this round: $score")
        RuleManager.totalScore += score

        RuleManager.selectNextDish()

        Game.endRound()
        gameHandler.postDelayed({
            EntityManager.unregisterEntity(scoreText)
            countDownToStart()
        }, 2000)
    }

    private fun startRound() {
        log("RuleManager: Started round")
        EntityCreator.createGameLayout(context!!)
        Game.run()
    }

    fun endGame() {
        if (Game.gameState == GameState.Ended) {
            log("RuleManager: Ended game")
            val score = RuleManager.scoreDish()
            EntityManager.unregisterAll()
            InteractionManager.handleUnselectSection()
            val scoreText = EntityCreator.createText(
                "",
                position = Vector2D(relX(1f / 2f), relY(1f / 2f)),
                align = Paint.Align.CENTER
            )
            RuleManager.totalScore += score
            scoreText.setTextValue("Final score: ${RuleManager.totalScore}")
            gameHandler.postDelayed({ Game.quit() }, 3000)
        }
    }
}