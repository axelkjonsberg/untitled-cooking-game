package com.wok_in_progress.rules

enum class GameState {
    Unset, Started, Running, Paused, EndedRound, StartedRound, Ended, Quit
}