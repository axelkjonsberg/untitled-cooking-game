package com.wok_in_progress.rules

import com.wok_in_progress.entities.EntityManager

object Timer {
    var maxTime = 80
    var timeLeft = maxTime

    @Synchronized
    fun updateTime(updatedTime: Int) {
        // Updates time left
        timeLeft = updatedTime
        EntityManager.timerText?.setTextValue("Time: ${timeLeft}s")
        // If time is up, change flag
        if (timeLeft <= 0) {
            Game.endGame()
        }
    }

    fun reset() {
        timeLeft = maxTime
    }
}