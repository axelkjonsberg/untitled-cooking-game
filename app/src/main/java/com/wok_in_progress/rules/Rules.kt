package com.wok_in_progress.rules

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.entities.Ingredient
import kotlin.math.floor

object Rules {
    private fun checkCorrectIngredients(input: List<Ingredient>, correct: List<Ingredient>): Int {
        var addition = 0
        val correctNames: MutableList<String> = mutableListOf()

        // Makes a list of strings from all the names in dish
        for (item in correct) {
            val toAdd = item.ruleBody?.name
            if (toAdd != null) {
                correctNames.add(toAdd)
            }
        }

        // Check if input ingredients is correct
        for (items in input) {
            if (items.ruleBody?.name in correctNames) {
                addition += 100
            }
            if (items.ruleBody?.name !in correctNames) {
                addition -= 20
            }
            correctNames.remove(items.ruleBody?.name)
        }

        return addition
    }

    private fun checkIfCorrectPrepared(input: List<Ingredient>, correct: List<Ingredient>): Int {
        var addition = 0
        val mutableCorrect: MutableList<Ingredient> = correct.toMutableList()

        for (item in input) {
            for (ingredient in mutableCorrect) {
                if (item.ruleBody?.isPrepared == ingredient.ruleBody?.isPrepared && item.ruleBody?.name == ingredient.ruleBody?.name) {
                    addition += 45
                    mutableCorrect.remove(ingredient)
                    break
                }
            }
        }

        return addition
    }

    private fun checkIfCorrectCooked(input: List<Ingredient>, correct: List<Ingredient>): Int {
        var addition = 0

        val mutableCorrect: MutableList<Entity> = correct.toMutableList()

        for (item in input) {
            for (ingredient in mutableCorrect) {
                if (floor(item.ruleBody!!.cooked) == ingredient.ruleBody?.cooked && item.ruleBody?.name == ingredient.ruleBody?.name) {
                    addition += 80
                    mutableCorrect.remove(ingredient)
                    break
                }
            }
        }
        return addition
    }

    private fun checkIfTouchedFloor(input: List<Ingredient>): Int {
        var subtraction = 0

        for (item in input) {
            if (item.ruleBody == null) continue
            if (item.ruleBody!!.isDirty) {
                subtraction -= 50
            }
        }
        return subtraction
    }

    private fun checkWaste(waste: List<Ingredient>): Int {
        var subtraction = 0
        for (item in waste) {
            subtraction -= 10
        }
        return subtraction
    }

    private fun checkTimeElapsed(): Int {
        return Timer.timeLeft
    }

    // Calculate score mode. Only one existing for now
    fun calculateScore(
        input: List<Ingredient>,
        waste: List<Ingredient>,
        correct: List<Ingredient>
    ): Int {
        var score = 0

        score += checkCorrectIngredients(input, correct)
        score += checkIfCorrectPrepared(input, correct)
        score += checkIfCorrectCooked(input, correct)
        score += checkIfTouchedFloor(input)
        score += checkWaste(waste)
        score += checkTimeElapsed()

        return score
    }
}