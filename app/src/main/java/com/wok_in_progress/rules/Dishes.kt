package com.wok_in_progress.rules

import com.wok_in_progress.entities.Ingredient

object Dishes {
    var dishes: LinkedHashMap<String, List<Ingredient>> = linkedMapOf() // List of dishes

    init {
        createDishes()
    }

    private fun createDishes() {
        dishes["Fried tomato"] = listOf(
            Ingredient(name = "tomato", cooked = 2f, isCut = true)
        )

        dishes["Fried tomato with egg"] = listOf(
            Ingredient(name = "egg", cooked = 2f),
            Ingredient(name = "tomato", isCut = true, cooked = 2f)
        )

        dishes["Burger"] = listOf(
            Ingredient(name = "hamburgerBunBottom", cooked = 1f),
            Ingredient(name = "patty", cooked = 2f),
            Ingredient(name = "cheese", cooked = 1f),
            Ingredient(name = "lettuce", cooked = 0f),
            Ingredient(name = "bacon", cooked = 2f),
            Ingredient(name = "hamburgerBunTop", cooked = 1f)
        )

        dishes["Steak"] = listOf(
            Ingredient(name = "steak", cooked = 2f)
        )
    }
}

