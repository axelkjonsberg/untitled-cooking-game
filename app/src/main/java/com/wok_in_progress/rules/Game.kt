package com.wok_in_progress.rules

import com.wok_in_progress.log
import kotlin.properties.Delegates.observable

object Game {
    private var onStateChanged: MutableSet<Pair<GameState, (() -> Unit)>> = mutableSetOf()

    var gameState: GameState by observable(GameState.Unset) { _, _, _ ->
        onStateChanged.forEach {
            if (it.first == gameState) {
                it.second.invoke()
            }
        }
    }

    fun subscribe(triggerState: GameState, callback: () -> Unit) {
        onStateChanged.add(Pair(triggerState, callback))
    }

    fun start() {
        when (gameState) {
            GameState.Unset -> {
                log("Game: Starting game")
                gameState = GameState.Started
            }
            GameState.EndedRound -> {
                log("Game: Starting round")
                gameState = GameState.StartedRound
            }
            else -> {
                log("Game: Attempted to start from illegal state ${gameState.name}")
            }
        }
    }

    fun run() {
        if (gameState in listOf(GameState.Started, GameState.StartedRound)) gameState =
            GameState.Running
        else {
            log("Game: Attempted to run game from illegal state ${gameState.name}")
        }
    }

    fun pause() {
        if (gameState in listOf(GameState.Running)) gameState = GameState.Paused
    }

    fun resume() {
        if (gameState in listOf(GameState.Paused)) gameState = GameState.Running
    }

    fun endRound() {
        if (gameState in listOf(GameState.Running)) gameState = GameState.EndedRound
        else {
            log("Game: Attempted to end round from illegal state ${gameState.name}")
        }
    }

    fun endGame() {
        if (gameState in listOf(GameState.Paused, GameState.Running)) {
            gameState = GameState.Ended
        }
    }

    fun quit() {
        log("Game: quit() run, state is $gameState")
        if (gameState in listOf(GameState.Running, GameState.Ended)) {
            log("Game: Set state to Quit")
            gameState = GameState.Quit
        }
    }

    fun reset() {
        log("Game: reset() run, state is $gameState")
        if (gameState in listOf(GameState.Quit, GameState.Paused)) {
            log("Game: Set state to Unset")
            gameState = GameState.Unset
        }
    }
}