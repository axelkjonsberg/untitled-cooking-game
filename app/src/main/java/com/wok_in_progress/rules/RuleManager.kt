package com.wok_in_progress.rules

import android.content.Context
import android.graphics.Paint
import android.os.Handler
import com.wok_in_progress.entities.EntityCreator
import com.wok_in_progress.entities.EntityManager
import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.interaction.InteractionManager
import com.wok_in_progress.log
import com.wok_in_progress.physics.PhysicsRunnable
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.GameTextures
import com.wok_in_progress.rendering.relX
import com.wok_in_progress.rendering.relY
import com.wok_in_progress.setWorldDimensions

object RuleManager {
    // I am the one who applies the game logic. You might also call me the Judge, the LogicEngine,
    // or something else more fitting of my role.
    private var delivered: List<Ingredient> = listOf()
    private var waste: List<Ingredient> = listOf()
    var index = getRandomIndex()
    var currentDish: MutableMap.MutableEntry<String, List<Ingredient>> =
        setCurrentDish("Fried tomato")
    var totalScore: Int = 0

    // currentDish is a map of dish name and a list of entities corresponding to the dish name
    // Access dish name by currentDish.key and dish entities by currentDish.value

    fun selectNextDish() {
        currentDish = nextDish()
    }

    fun scoreDish(): Int {
        val deliveredDish: List<Ingredient>? =
            EntityManager.deliverySection?.interactable?.contains?.filterIsInstance<Ingredient>()
        val fetched: List<Ingredient>? = EntityManager.spawnFridge?.ingredientRemovedFromFridge
        return if (deliveredDish != null && fetched != null) {
            setInput(deliveredDish = deliveredDish, fetched = fetched)
            getScore()
        } else 0
    }

    fun setInput(deliveredDish: List<Ingredient>, fetched: List<Ingredient>) {
        delivered = deliveredDish
        waste = fetched.filter { ingredient -> ingredient !in deliveredDish }
    }

    // Usage: RuleManager.currentDish = RuleManager.setCurrentDish("NAME OF DISH")
    private fun setCurrentDish(name: String): MutableMap.MutableEntry<String, List<Ingredient>> {
        val keys: MutableSet<String> = Dishes.dishes.keys
        val tempIndex = keys.indexOf(name)
        return Dishes.dishes.entries.elementAt(tempIndex)
    }

    // Usage: RuleManager.currentDish = RuleManager.nextDish()
    fun nextDish(): MutableMap.MutableEntry<String, List<Ingredient>> {
        val nextIndex = index + 1
        if (nextIndex < Dishes.dishes.size) {
            index = nextIndex
            return Dishes.dishes.entries.elementAt(index)
        }
        index = 0
        return Dishes.dishes.entries.elementAt(index)
    }

    private fun getRandomIndex(): Int {
        val size = Dishes.dishes.size - 1
        return (0..size).random()
    }

    // Usage: RuleManager.currentDish = RuleManager.randomDish()
    private fun randomDish(): MutableMap.MutableEntry<String, List<Ingredient>> {
        index = getRandomIndex()
        return Dishes.dishes.entries.elementAt(index)
    }

    fun getScore(): Int {
        return Rules.calculateScore(delivered, waste, currentDish.value)
    }
}


