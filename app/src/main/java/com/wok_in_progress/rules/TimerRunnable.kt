package com.wok_in_progress.rules

import android.os.Handler
import com.wok_in_progress.log

/**
 * This runnable handles the timing of a game session.
 */

class TimerRunnable(val handler: Handler) : Runnable {
    private var currentTime = 0
    private var paused = false
    private var running = false

    init {
        Game.subscribe(GameState.Running, ::resume)
        Game.subscribe(GameState.Paused, ::pause)
        Game.subscribe(GameState.Unset, ::reset)
    }

    private fun resume() {
        paused = false
        handler.postDelayed(this, 1000)
    }

    private fun pause() {
        paused = true
    }

    private fun reset() {
        currentTime = 0
    }

    override fun run() {
        if (!running && !paused && Game.gameState == GameState.Running) {
            running = true
            currentTime++

            // Update the timer
            Timer.updateTime(Timer.maxTime - currentTime)
            running = false
            if (Game.gameState != GameState.Ended) handler.postDelayed(this, 1000)
        } else log("TimerRunnable: Decided not to run")
    }
}