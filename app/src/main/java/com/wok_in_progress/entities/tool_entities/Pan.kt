package com.wok_in_progress.entities.tool_entities

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Pan(position: Vector2D) : Entity(
    renderable = Sprite(
        position = position,
        dimensions = Dimensions(TextureSizes.panSize, TextureSizes.panHeight),
        textureName = "pan",
        layer = 1
    ),
    physicalBody = PhysicalBody(
        position = position,
        dimensions = Dimensions(TextureSizes.panSize, TextureSizes.panHeight),
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(
                    position.x - (TextureSizes.panSize / 2f),
                    position.y - (TextureSizes.panHeight / 2f)
                ),
                dimensions = Dimensions(TextureSizes.panSize * (9f / 14f), TextureSizes.panHeight),
                dx = -(TextureSizes.panSize / 2f),
                dy = -(TextureSizes.panHeight / 2f)
            )
        ),
        mass = 3500f, // Cast iron pan weight
        isContainer = true
    ),
    interactable = Interactable(movable = true, cooks = true)
)