package com.wok_in_progress.entities.tool_entities

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Knife(position: Vector2D) : Entity(
    renderable = Sprite(
        position = position,
        dimensions = Dimensions(TextureSizes.knifeSize, TextureSizes.knifeHeight),
        layer = 2,
        textureName = "knife"
    ),
    physicalBody = PhysicalBody(
        position = position,
        dimensions = Dimensions(TextureSizes.knifeSize, TextureSizes.knifeHeight),
        mass = 263f, // From https://www.knivesandtools.com/en/pt/-classic-cook-s-knife-8.htm
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(
                    position.x - TextureSizes.knifeSize / 2f,
                    position.y - TextureSizes.knifeHeight / 2f
                ),
                dimensions = Dimensions(TextureSizes.knifeSize, TextureSizes.knifeHeight),
                dx = -TextureSizes.knifeSize / 2f,
                dy = -TextureSizes.knifeHeight / 2f
            )
        )
    ),
    interactable = Interactable(cuts = true)
)