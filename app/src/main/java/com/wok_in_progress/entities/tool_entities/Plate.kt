package com.wok_in_progress.entities.tool_entities

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Plate(
    position: Vector2D,
    dimensions: Dimensions,
    zoomable: Boolean = true
) : Entity(
    renderable = Sprite(
        position = position,
        dimensions = Dimensions(TextureSizes.plateSize, TextureSizes.plateHeight),
        layer = 1,
        textureName = "plate",
        zoomable = zoomable,
        drawCentered = true
    ),
    physicalBody = PhysicalBody(
        position = position,
        dimensions = dimensions,
        movable = false,
        isContainer = true,
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(
                    position.x - (TextureSizes.plateSize / 2f),
                    position.y - (TextureSizes.plateHeight / 1f)
                ),
                dimensions = Dimensions(TextureSizes.plateSize, TextureSizes.plateHeight),
                dx = -(TextureSizes.plateSize / 2f),
                dy = -(TextureSizes.plateHeight / 1f)
            )
        )
    ),
    interactable = Interactable(contains = mutableListOf(), movable = false)
)