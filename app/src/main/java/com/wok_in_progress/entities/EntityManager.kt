package com.wok_in_progress.entities

import com.wok_in_progress.entities.layout_entities.DeliverySection
import com.wok_in_progress.entities.layout_entities.Fridge
import com.wok_in_progress.entities.layout_entities.TextEntity
import com.wok_in_progress.physics.PhysicsEngine
import com.wok_in_progress.rendering.Renderer

object EntityManager {
    var entities: LinkedHashSet<Entity> = linkedSetOf()
    var deliverySection: DeliverySection? = null
    var timerText: TextEntity? = null
    var debug = false
    var worldWidth = 0f
    var worldHeight = 0f
    var spawnFridge: Fridge? = null

    fun setWorldDimensions(width: Float, height: Float) {
        worldWidth = width
        worldHeight = height
    }

    fun registerEntity(
        entity: Entity,
        inRenderer: Boolean = true,
        inPhysicsEngine: Boolean = true
    ) {
        entities.add(entity)
        if (entity.physicalBody != null && inPhysicsEngine) PhysicsEngine.registerEntity(
            entity
        )
        if (entity.renderable != null && inRenderer) Renderer.registerRenderable(entity.renderable!!)
        if (entity.renderables != null && inRenderer) entity.renderables!!.forEach {
            Renderer.registerRenderable(
                it
            )
        }
        if (debug && entity !is DebugHitbox && inRenderer) EntityCreator.createDebugHitboxes(entity)
    }

    fun unregisterEntity(entity: Entity?) {
        if (entity != null) {
            if (entity.physicalBody != null) {
                entity.debugHitboxes.forEach { unregisterEntity(it) }
                PhysicsEngine.unregisterEntity(entity)
            }
            if (entity.renderable != null) Renderer.unregisterRenderable(entity.renderable!!)
            entity.renderables?.forEach { Renderer.unregisterRenderable(it) }
            entity.interactable?.reset()
            entities.remove(entity)
        }
    }

    fun unregisterAll() {
        val entitiesToUnregister: MutableList<Entity?> = entities.toMutableList()
        entitiesToUnregister.add(deliverySection as Entity?)
        deliverySection?.interactable?.reset()
        entitiesToUnregister.forEach { unregisterEntity(it) }
    }

}