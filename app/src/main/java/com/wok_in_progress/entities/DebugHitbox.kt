package com.wok_in_progress.entities

import android.graphics.Color
import android.graphics.Paint
import com.wok_in_progress.physics.CircleHitbox
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.rendering.renderables.Circle
import com.wok_in_progress.rendering.renderables.Rectangle

class DebugHitbox(
    var rectangleHitbox: RectangleHitbox? = null,
    var circleHitbox: CircleHitbox? = null,
    color: Int = Color.BLUE
) : Entity(
    renderable = when {
        rectangleHitbox != null -> Rectangle(
            color,
            Paint.Style.STROKE,
            rectangleHitbox
        )
        circleHitbox != null -> Circle(
            color,
            Paint.Style.STROKE,
            circleHitbox
        )
        else -> null
    }
)