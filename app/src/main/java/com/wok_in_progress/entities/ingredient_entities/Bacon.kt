package com.wok_in_progress.entities.ingredient_entities

import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Bacon(
    position: Vector2D,
    velocity: Vector2D = Vector2D(0f, 0f),
    textureName: String = "bacon",
    cookedTextureName: String = "bacon_cooked"
) : Ingredient(
    name = textureName,
    cookedTextureName = cookedTextureName,
    renderable = Sprite(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.baconSize, TextureSizes.baconHeight),
        rotation = 0f,
        textureName = textureName,
        layer = 2
    ),
    physicalBody = PhysicalBody(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.baconSize, TextureSizes.baconHeight),
        velocity = velocity,
        mass = 180f, // From https://hannaone.com/Recipe/weighttomato.html
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(
                    position.x - TextureSizes.baconSize / 2f,
                    position.y - TextureSizes.baconHeight / 2f
                ),
                dimensions = Dimensions(TextureSizes.baconSize, TextureSizes.baconHeight),
                dx = -TextureSizes.baconSize / 2f,
                dy = -TextureSizes.baconHeight / 2f
            )
        )
    ),
    interactable = Interactable(cookable = true)
)