package com.wok_in_progress.entities.ingredient_entities

import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Steak(
    position: Vector2D,
    textureName: String = "steak",
    width: Float = TextureSizes.steakSize,
    height: Float = TextureSizes.steakHeight
) : Ingredient(
    name = textureName,
    renderable = Sprite(
        position = Vector2D(position),
        dimensions = Dimensions(width, height),
        rotation = 0f,
        textureName = textureName,
        layer = 2
    ),
    physicalBody = PhysicalBody(
        position = Vector2D(position),
        dimensions = Dimensions(width, height),
        mass = 400f, // From https://hannaone.com/Recipe/weighttomato.html
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(position.x - width / 2f, position.y - height / 3f),
                dimensions = Dimensions(width, height / 2f),
                dx = -width / 2f,
                dy = -height / 3f
            )
        )
    ),
    interactable = Interactable(cookable = true)
)