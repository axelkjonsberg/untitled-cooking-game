package com.wok_in_progress.entities.ingredient_entities

import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class HamburgerBunBottom(
    position: Vector2D,
    textureName: String = "hamburgerBunBottom",
    width: Float = TextureSizes.hamburgerBunBottomSize,
    height: Float = TextureSizes.hamburgerBunBottomHeight
) : Ingredient(
    name = textureName,
    renderable = Sprite(
        position = Vector2D(position),
        dimensions = Dimensions(width, height),
        rotation = 0f,
        textureName = textureName,
        layer = 2
    ),
    physicalBody = PhysicalBody(
        position = Vector2D(position),
        dimensions = Dimensions(width, height),
        mass = 46f, // From https://answers.yahoo.com/question/index?qid=20080715133855AAy5zwG
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(position.x - width / 2f, position.y - height / 2f),
                dimensions = Dimensions(width, height),
                dx = -width / 2f,
                dy = -height / 2f
            )
        )
    ),
    interactable = Interactable(cookable = true)
)