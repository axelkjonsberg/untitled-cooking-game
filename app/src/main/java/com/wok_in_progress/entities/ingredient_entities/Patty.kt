package com.wok_in_progress.entities.ingredient_entities

import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Patty(
    position: Vector2D,
    width: Float = TextureSizes.pattySize,
    height: Float = TextureSizes.pattyHeight,
    textureName: String = "patty",
    cookedTextureName: String = "patty_cooked"
) : Ingredient(
    name = textureName,
    cookedTextureName = cookedTextureName,
    renderable = Sprite(
        position = position,
        dimensions = Dimensions(width, height),
        layer = 2,
        textureName = textureName
    ),
    physicalBody = PhysicalBody(
        position = Vector2D(position),
        dimensions = Dimensions(width, height),
        mass = 200f,
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(position.x - width / 2f, position.y - height / 2f),
                dimensions = Dimensions(width, height),
                dx = - width / 2f,
                dy = - height / 2f
            )
        )
    ),
    interactable = Interactable(cookable = true)
)