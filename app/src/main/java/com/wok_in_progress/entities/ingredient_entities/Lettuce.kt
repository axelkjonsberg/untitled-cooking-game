package com.wok_in_progress.entities.ingredient_entities

import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Lettuce(
    position: Vector2D,
    textureName: String = "lettuce"
) : Ingredient(
    name = textureName,
    renderable = Sprite(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.lettuceSize, TextureSizes.lettuceHeight),
        rotation = 0f,
        textureName = textureName,
        layer = 2,
        drawCentered = true
    ),
    physicalBody = PhysicalBody(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.lettuceSize, TextureSizes.lettuceHeight),
        mass = 24f, // From https://nutritiondata.self.com/facts/vegetables-and-vegetable-products/2477/2
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(
                    position.x - TextureSizes.lettuceSize / 2f,
                    position.y - TextureSizes.lettuceHeight / 2f
                ),
                dimensions = Dimensions(TextureSizes.lettuceSize, TextureSizes.lettuceHeight),
                dx = -TextureSizes.lettuceSize / 2f,
                dy = -TextureSizes.lettuceHeight / 2f
            )
        )
    ),
    interactable = Interactable(cookable = true)
)