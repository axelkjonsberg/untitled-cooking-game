package com.wok_in_progress.entities.ingredient_entities

import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.CircleHitbox
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite


// To avoid saving a huge amount of textures, all instances use the same texture from GameTextures
class Tomato(
    position: Vector2D,
    textureName: String = "tomato",
    cutTextureName: String = "tomato_sliced",
    cutCookedTextureName: String = "tomato_sliced_cooked"
) : Ingredient(
    name = textureName,
    cutTextureName = cutTextureName,
    cutCookedTextureName = cutCookedTextureName,
    renderable = Sprite(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.tomatoSize),
        textureName = textureName,
        layer = 2
    ),
    physicalBody = PhysicalBody(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.tomatoSize),
        mass = 180f, // From https://hannaone.com/Recipe/weighttomato.html
        circleHitboxes = listOf(
            CircleHitbox(
                position = Vector2D(position),
                radius = TextureSizes.tomatoSize / 2,
                dx = 0f,
                dy = 0f
            )
        )
    ),
    interactable = Interactable(cuttable = true, cookable = true)
)