package com.wok_in_progress.entities.ingredient_entities

import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.CircleHitbox
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Egg(
    position: Vector2D,
    textureName: String = "egg"
) : Ingredient(
    name = textureName,
    renderable = Sprite(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.eggSize, TextureSizes.eggHeight),
        rotation = 0f,
        textureName = textureName,
        layer = 2
    ),
    physicalBody = PhysicalBody(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.eggSize, TextureSizes.eggHeight),
        mass = 63f, // From https://www.egginfo.co.uk/egg-facts-and-figures/industry-information/egg-sizes
        circleHitboxes = listOf(
            CircleHitbox(
                position = Vector2D(position.x, position.y - TextureSizes.eggHeight / 4f),
                radius = TextureSizes.eggHeight * (8f / 32f),
                dx = 0f,
                dy = -TextureSizes.eggHeight / 4f
            ),
            CircleHitbox(
                position = Vector2D(position.x, position.y + TextureSizes.eggHeight / 4f),
                radius = TextureSizes.eggHeight * (8f / 32f),
                dx = 0f,
                dy = TextureSizes.eggHeight / 4f
            ),
            CircleHitbox(
                position = Vector2D(position.x, position.y),
                radius = TextureSizes.eggSize * (16f / 32f),
                dx = 0f,
                dy = 0f
            )
        )
    ),
    interactable = Interactable(cookable = true)
)