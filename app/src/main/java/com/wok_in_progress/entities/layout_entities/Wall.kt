package com.wok_in_progress.entities.layout_entities

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.physics.*

class Wall(
    position: Vector2D,
    dimensions: Dimensions,
    pushDirection: Direction
) : Entity(
    physicalBody = PhysicalBody(
        position = position,
        dimensions = dimensions,
        rectangleHitboxes = listOf(RectangleHitbox(position, dimensions, 0f, 0f)),
        movable = false,
        isWall = true,
        pushDirection = pushDirection,
        isStatic = true
    )
)