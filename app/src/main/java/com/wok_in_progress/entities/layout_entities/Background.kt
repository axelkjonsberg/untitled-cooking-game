package com.wok_in_progress.entities.layout_entities

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import androidx.core.content.ContextCompat
import com.wok_in_progress.R
import com.wok_in_progress.entities.Entity
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.renderables.Rectangle

class Background(
    dimensions: Dimensions,
    context: Context
) : Entity(
    renderables = listOf(
        Rectangle(
            color = ContextCompat.getColor(context, R.color.colorPrimary),
            style = Paint.Style.FILL_AND_STROKE,
            position = Vector2D(),
            dimensions = Dimensions(dimensions.width, dimensions.height * (29f / 64f)),
            layer = 0
        ),
        Rectangle(
            color = ContextCompat.getColor(context, R.color.colorAccent),
            style = Paint.Style.FILL_AND_STROKE,
            position = Vector2D(0f, dimensions.height * (29f / 64f)),
            dimensions = Dimensions(dimensions.width, dimensions.height * (35f / 64f)),
            layer = 0
        ),
        Rectangle(
            color = Color.WHITE,
            style = Paint.Style.FILL_AND_STROKE,
            position = Vector2D(0f, dimensions.height * (57f / 128f)),
            dimensions = Dimensions(dimensions.width, dimensions.height * (5f / 512f)),
            layer = 0
        )
    )
)