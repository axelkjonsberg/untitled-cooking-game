package com.wok_in_progress.entities.layout_entities

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class DeliverySection(
    position: Vector2D
) : Entity(
    renderable = Sprite(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.plateSize, TextureSizes.plateHeight),
        layer = 1,
        textureName = "deliveryplate"
    ),
    physicalBody = PhysicalBody(
        position = Vector2D(position),
        dimensions = Dimensions(TextureSizes.plateSize),
        movable = false,
        isContainer = true,
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(
                    position.x - TextureSizes.plateSize / 2f,
                    position.y - TextureSizes.plateHeight * 2f
                ),
                dimensions = Dimensions(TextureSizes.plateSize, TextureSizes.plateHeight * 2f),
                dx = -TextureSizes.plateSize / 2f,
                dy = -TextureSizes.plateHeight * 2f
            )
        )
    ),
    interactable = Interactable(contains = mutableListOf(), movable = false)
)