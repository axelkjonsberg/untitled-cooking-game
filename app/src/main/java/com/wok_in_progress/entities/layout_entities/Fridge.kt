package com.wok_in_progress.entities.layout_entities

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.entities.EntityManager
import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class Fridge(position: Vector2D, ingredients: List<Ingredient>) : Entity(
    renderable = Sprite(
        position = position,
        dimensions = Dimensions(TextureSizes.fridgeSize, TextureSizes.fridgeHeight),
        rotation = 0f,
        textureName = "fridge",
        layer = 1,
        drawCentered = true
    ),
    physicalBody = PhysicalBody(
        position = position,
        dimensions = Dimensions(TextureSizes.fridgeSize, TextureSizes.fridgeHeight),
        rectangleHitboxes = listOf(
            RectangleHitbox(
                Vector2D(
                    position.x - TextureSizes.fridgeSize / 2f,
                    position.y - TextureSizes.fridgeHeight / 2f
                ),
                Dimensions(TextureSizes.fridgeSize, TextureSizes.fridgeHeight),
                dx = -TextureSizes.fridgeSize / 2f,
                dy = -TextureSizes.fridgeHeight / 2f
            )
        ),
        movable = false,
        collidable = false
    )
) {
    var ingredientsInFridge: MutableList<Ingredient> =
        mutableListOf()     // Keep tracks of ingredients not removed from fridge
    var ingredientRemovedFromFridge: MutableList<Ingredient> = mutableListOf()
    private var availableIngredients: List<Ingredient> = ingredients

    //indicates if fridge is open
    var open = false
    var firstTime = true

    //open_closes door
    fun toggleOpen() {
        open = !open

        // If this is the first time, fill the fridge
        if (firstTime) {
            firstTime = false
            for (ingredient in availableIngredients) {
                // Ensure that they are static
                ingredient.physicalBody!!.isStatic = true
                ingredientsInFridge.add(ingredient)
            }
        }

        if (open) {
            // Change texture
            (renderable as Sprite).textureName = "fridge_open"
            //Spawn ingredients
            spawnInFridge()
        } else {
            // Change texture
            (renderable as Sprite).textureName = "fridge"
            // unregister entities
            closeFridge()
        }
    }

    //Spawns ingredients in fridge when the door is opened
    private fun spawnInFridge() {
        // Re-register the entities in rendering and physics
        for (ingredient in ingredientsInFridge) {
            EntityManager.registerEntity(ingredient)
        }
    }


    // Removes the ingredient from the fridges list and adds a copy
    fun removeEntityFromFridge(ingredient: Ingredient? = null, entity: Entity? = null) {
        if (entity != null) return
        if (ingredient == null) return
        // Remove from the fridge list
        ingredientsInFridge.remove(ingredient)

        //Spawn identical element
        val ingredientCopy: Ingredient = ingredient.copy()
        ingredientCopy.physicalBody!!.isStatic = true
        EntityManager.registerEntity(ingredientCopy)
        ingredientsInFridge.add(ingredientCopy)

        // Remove static from old element
        ingredient.physicalBody!!.isStatic = false

        ingredientRemovedFromFridge.add(ingredient)
    }

    // Unregisters all elements in ingredientsInFridge from rendering and physics
    private fun closeFridge() {
        for (ingredient in ingredientsInFridge) {
            EntityManager.unregisterEntity(ingredient)
        }
    }
}