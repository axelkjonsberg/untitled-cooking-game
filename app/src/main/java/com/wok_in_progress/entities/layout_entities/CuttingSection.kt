package com.wok_in_progress.entities.layout_entities

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.renderables.Sprite

class CuttingSection(position: Vector2D) : Entity(
    renderable = Sprite(
        textureName = "cuttingSection",
        position = position,
        dimensions = Dimensions(TextureSizes.cuttingSectionSize, TextureSizes.cuttingSectionHeight),
        layer = 0,
        drawCentered = true
    )
)