package com.wok_in_progress.entities.layout_entities

import android.graphics.Color
import android.graphics.Paint
import com.wok_in_progress.entities.Entity
import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.renderables.Rectangle
import com.wok_in_progress.rendering.renderables.Text

class Button(
    position: Vector2D,
    dimensions: Dimensions,
    color: Int = Color.RED,
    buttonRounding: Float = 0f,
    textColor: Int = Color.WHITE,
    textSize: Float = 16f,
    textValue: String = "Button",
    callback: (() -> Unit)? = null,
    zoomable: Boolean = true
) : Entity(
    physicalBody = PhysicalBody(
        position = position,
        dimensions = dimensions,
        rectangleHitboxes = listOf(
            RectangleHitbox(
                position = Vector2D(position),
                dimensions = dimensions,
                dx = 0f,
                dy = 0f
            )
        ),
        movable = false,
        collidable = false
    ),
    renderables = listOf(
        Rectangle(
            color = color,
            rounding = buttonRounding,
            rectangleHitbox = RectangleHitbox(
                position = position,
                dimensions = dimensions,
                dx = 0f,
                dy = 0f
            ),
            style = Paint.Style.FILL_AND_STROKE,
            zoomable = zoomable
        ),
        Text(
            position = Vector2D(
                position.x + (dimensions.width / 2f),
                position.y + (dimensions.height / 2f) + (textSize / 4f)
            ),
            value = textValue,
            color = textColor,
            size = textSize,
            zoomable = zoomable,
            align = Paint.Align.CENTER
        )
    ),
    interactable = Interactable(callback = callback, movable = false)
) {
    override fun updatePosition(position: Vector2D) {
        // Do nothing
    }
}
