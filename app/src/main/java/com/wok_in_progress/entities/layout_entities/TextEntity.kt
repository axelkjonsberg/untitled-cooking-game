package com.wok_in_progress.entities.layout_entities

import android.graphics.Paint
import com.wok_in_progress.entities.Entity
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.renderables.Text

class TextEntity(
    position: Vector2D,
    value: String = "Empty text",
    align: Paint.Align = Paint.Align.LEFT,
    zoomable: Boolean = true
) : Entity(
    renderable = Text(
        position = position,
        value = value,
        align = align,
        zoomable = zoomable
    )
) {
    fun setTextValue(newValue: String) {
        (renderable as Text).value = newValue
    }
}