package com.wok_in_progress.entities

import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.rendering.renderables.Renderable
import com.wok_in_progress.rendering.renderables.Sprite
import com.wok_in_progress.rules.RuleBody
import kotlin.math.min

open class Ingredient(
    name: String = "UNSET_INGREDIENT_NAME",
    isPrepared: Boolean = false,
    isCut: Boolean = false,
    isDirty: Boolean = false,
    cooked: Float = 0f,
    open val cutTextureName: String? = null,
    open val cookedTextureName: String? = null,
    open val cutCookedTextureName: String? = null,
    ruleBody: RuleBody? = null,
    renderable: Renderable? = null,
    physicalBody: PhysicalBody? = null,
    interactable: Interactable? = null
) : Entity(
    ruleBody = ruleBody ?: RuleBody(
        name = name,
        isPrepared = isPrepared,
        isCut = isCut,
        isDirty = isDirty,
        cooked = cooked
    ),
    renderable = renderable,
    physicalBody = physicalBody,
    interactable = interactable
) {

    override fun copy(): Ingredient {
        val copiedEntity = super.copy()
        return Ingredient(
            cutTextureName = cutTextureName,
            cookedTextureName = cookedTextureName,
            cutCookedTextureName = cutCookedTextureName,
            ruleBody = copiedEntity.ruleBody,
            renderable = copiedEntity.renderable,
            physicalBody = copiedEntity.physicalBody,
            interactable = copiedEntity.interactable
        )
    }

    open fun cut() {
        val wasCut = ruleBody?.isCut
        ruleBody?.cut()
        if (renderable != null && cutTextureName != null && renderable is Sprite) {
            (renderable as Sprite).textureName = cutTextureName as String
            val cooked = (renderable as Sprite).cooked
            if (cutCookedTextureName != null && cooked >= 2f) {
                (renderable as Sprite).textureName = cutCookedTextureName as String
            }
        }
        if (wasCut == false && cutTextureName != null) EntityManager.registerEntity(copy())
    }

    open fun cook() {
        ruleBody?.cook()
        if (renderable != null
            && renderable is Sprite
            && ruleBody != null
            && interactable != null
            && interactable!!.cookable
        ) {
            (renderable as Sprite).cooked = min((ruleBody!!.cooked), 3f)
            if ((renderable as Sprite).cooked >= 2f) {
                if (cutCookedTextureName != null && ruleBody?.isCut == true) {
                    (renderable as Sprite).textureName = cutCookedTextureName as String
                }
                else if (cookedTextureName != null) {
                    (renderable as Sprite).textureName = cookedTextureName as String
                }
            }

        }
    }

    open fun makeDirty() {
        ruleBody?.makeDirty()
    }

    open fun clean() {
        ruleBody?.clean()
    }

}