package com.wok_in_progress.entities

import com.wok_in_progress.interaction.Interactable
import com.wok_in_progress.physics.PhysicalBody
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.renderables.*
import com.wok_in_progress.rules.RuleBody

open class Entity(
    open var physicalBody: PhysicalBody? = null,
    open var renderable: Renderable? = null,
    var renderables: List<Renderable>? = null,
    var ruleBody: RuleBody? = null,
    open var interactable: Interactable? = null,
    var debugHitboxes: MutableList<Entity> = mutableListOf()
) {
    open fun copy(): Entity {
        val copiedPhysicalBody = if (physicalBody != null) PhysicalBody(physicalBody!!) else null
        val copiedRenderable = copyRenderable()
        val copiedRenderables = renderables?.map { copyRenderable(it)!! }
        val copiedRuleBody = if (ruleBody != null) RuleBody(ruleBody!!) else null
        val copiedInteractable = if (interactable != null) Interactable(interactable!!) else null
        return Entity(
            physicalBody = copiedPhysicalBody,
            renderable = copiedRenderable,
            renderables = copiedRenderables,
            ruleBody = copiedRuleBody,
            interactable = copiedInteractable
        )
    }

    private fun copyRenderable(renderable: Renderable?): Renderable? {
        return when {
            renderable != null && renderable is Sprite -> Sprite(renderable)
            renderable != null && renderable is Circle -> Circle(renderable)
            renderable != null && renderable is Rectangle -> Rectangle(renderable)
            renderable != null && renderable is Text -> Text(renderable)
            else -> null
        }
    }

    private fun copyRenderable(): Renderable? {
        return when {
            renderable != null && renderable is Sprite -> Sprite(renderable as Sprite)
            renderable != null && renderable is Circle -> Circle(renderable as Circle)
            renderable != null && renderable is Rectangle -> Rectangle(renderable as Rectangle)
            renderable != null && renderable is Text -> Text(renderable as Text)
            else -> null
        }
    }

    open fun updatePosition(position: Vector2D) {
        if (renderable != null) {
            renderable!!.updatePosition(position)
        }
        if (renderables != null) {
            renderables!!.forEach { it.updatePosition(position) }
        }
        if (physicalBody != null) {
            if (interactable != null && interactable!!.held) {
                physicalBody!!.updateVelocity(Vector2D())
            }
            physicalBody!!.updatePosition(position)
        }
        if (debugHitboxes.size > 0) {
            debugHitboxes.forEach {
                it.renderable?.updatePosition(
                    (it as DebugHitbox).rectangleHitbox?.position
                        ?: it.circleHitbox?.position ?: position
                )
            }
        }
    }

    open fun updatePosition() {
        if (physicalBody != null) {
            if (interactable != null && interactable!!.held) {
                physicalBody!!.updateVelocity(Vector2D())
            }
            physicalBody!!.updatePosition()
            val newPos = Vector2D(physicalBody!!.position)
            updatePosition(newPos)
        }
    }

    fun updateHeld(fingerIndex: Int) {
        if (interactable != null) {
            interactable!!.held = true
            interactable!!.heldBy = fingerIndex
            if (renderable != null) {
                if (renderable is Sprite) {
                    (renderable as Sprite).hasOutline = true
                }
            }
        }
    }

    fun removeHeld() {
        if (interactable != null) {
            interactable!!.held = false
            if (renderable != null) {
                if (renderable is Sprite) {
                    (renderable as Sprite).hasOutline = false
                }
            }
        }
    }
}