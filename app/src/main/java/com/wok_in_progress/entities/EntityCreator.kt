package com.wok_in_progress.entities

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import com.wok_in_progress.entities.ingredient_entities.*
import com.wok_in_progress.entities.layout_entities.*
import com.wok_in_progress.entities.tool_entities.Knife
import com.wok_in_progress.entities.tool_entities.Pan
import com.wok_in_progress.entities.tool_entities.Plate
import com.wok_in_progress.interaction.InteractionManager
import com.wok_in_progress.log
import com.wok_in_progress.physics.*
import com.wok_in_progress.rendering.TextureSizes
import com.wok_in_progress.rendering.relX
import com.wok_in_progress.rendering.relY
import com.wok_in_progress.rules.Timer

object EntityCreator {
    private var worldWidth = 0f
    private var worldHeight = 0f

    fun createGameLayout(context: Context) {
        log("EntityCreator: Creating entities")
        worldWidth = EntityManager.worldWidth
        worldHeight = EntityManager.worldHeight
        EntityManager.unregisterAll() // Need to make sure there are no leftovers if this is run twice
        EntityManager.entities = linkedSetOf()
        log("EntityCreator: Worldwidth: ${worldWidth}, worldheight: $worldHeight")

        // Layout
        EntityManager.registerEntity(Background(Dimensions(relX(1f), relY(1f)), context))
        createWorldWalls()
        createDeliverySection(
            platePosition = Vector2D(
                worldWidth * (2f / 16f),
                worldHeight * (95f / 256f)
            )
        )
        EntityManager.registerEntity(CuttingSection(Vector2D(relX(5f / 8f), relY(91f / 256f))))
        EntityManager.registerEntity(DeliveryTable(Vector2D(relX(1f / 8f), relY(91f / 256f))))
        createOven(Vector2D(relX(3f / 8f), relY(91f / 256f)))
        createFridge(Vector2D(relX(7f / 8f), relY(92f / 256f)))
        createKnife(Vector2D(relX(5f / 8f), relY(1f / 4f)))
        InteractionManager.plate =
            createPlate(Vector2D(worldWidth * (14f / 16f), worldHeight * (15 / 32f)))
        createPan(Vector2D(relX(3f / 8f), relY(1f / 4f)))
        // Walls for section 1
        createWall(
            position = Vector2D(0f, worldHeight * (6f / 16f)),
            dimensions = Dimensions(worldWidth / 4f, worldHeight * (0.02f)),
            pushDirection = Direction.UP
        )
        createWall(
            position = Vector2D(0f, worldHeight * (63f / 128f)),
            dimensions = Dimensions(worldWidth / 4f, worldHeight * (0.02f)),
            pushDirection = Direction.UP
        )
        // Walls for section 2
        createWall(
            position = Vector2D(worldWidth * (1f / 4f), worldHeight * (6f / 16f)),
            dimensions = Dimensions(worldWidth / 4f, worldHeight * (0.02f)),
            pushDirection = Direction.UP
        )
        createWall(
            position = Vector2D(worldWidth * (1f / 4f), worldHeight * (63f / 128f)),
            dimensions = Dimensions(worldWidth / 4f, worldHeight * (0.02f)),
            pushDirection = Direction.UP
        )
        // Walls for section 3
        createWall(
            position = Vector2D(worldWidth * (2f / 4f), worldHeight * (6f / 16f)),
            dimensions = Dimensions(worldWidth / 4f, worldHeight * (0.02f)),
            pushDirection = Direction.UP
        )
        createWall(
            position = Vector2D(worldWidth * (2f / 4f), worldHeight * (63f / 128f)),
            dimensions = Dimensions(worldWidth / 4f, worldHeight * (0.02f)),
            pushDirection = Direction.UP
        )
        // Walls for section 4
        /*createWall(
            position = Vector2D(worldWidth * (3f / 4f), worldHeight * (6f / 16f)),
            dimensions = Dimensions(worldWidth / 4f, worldHeight * (0.02f)),
            pushDirection = Direction.UP
        )*/
        createWall(
            position = Vector2D(worldWidth * (3f / 4f), worldHeight * (63f / 128f)),
            dimensions = Dimensions(worldWidth / 4f, worldHeight * (0.02f)),
            pushDirection = Direction.UP
        )

        // Text
        createTimerText()
    }

    fun createPerformanceTest() {
        worldWidth = EntityManager.worldWidth
        worldHeight = EntityManager.worldHeight
        EntityManager.unregisterAll() // Need to make sure there are no leftovers if this is run twice
        EntityManager.entities = linkedSetOf()
        createWorldWalls(includeTop = true)
        val numObjects = 100
        for (i in 1..numObjects) {
            EntityManager.registerEntity(
                Tomato(
                    position = Vector2D(
                        x = i % worldWidth,
                        y = i % worldHeight
                    )
                )
            )
        }
    }

    fun createButton(
        position: Vector2D,
        dimensions: Dimensions,
        color: Int = Color.RED,
        textColor: Int = Color.WHITE,
        buttonRounding: Float = 32f,
        textValue: String = "Button",
        callback: (() -> Unit)? = null
    ): Button {
        val button = Button(
            position = position,
            dimensions = dimensions,
            color = color,
            textColor = textColor,
            textValue = textValue,
            callback = callback,
            buttonRounding = buttonRounding
        )
        EntityManager.registerEntity(button)
        return button
    }

    fun createText(
        textValue: String,
        position: Vector2D,
        align: Paint.Align = Paint.Align.LEFT,
        zoomable: Boolean = true
    ): TextEntity {
        val text =
            TextEntity(
                position = position,
                value = textValue,
                align = align,
                zoomable = zoomable
            )
        EntityManager.registerEntity(text)
        return text
    }

    private fun createTimerText() {
        EntityManager.timerText = createText(
            textValue = "Time: ${Timer.timeLeft}s",
            position = Vector2D(
                EntityManager.worldWidth * 0.99f,
                EntityManager.worldHeight * 0.03f
            ),
            zoomable = false,
            align = Paint.Align.RIGHT
        )
    }

    private fun createKnife(position: Vector2D) {
        EntityManager.registerEntity(Knife(position = position))
    }

    private fun createPlate(
        position: Vector2D,
        dimensions: Dimensions = Dimensions(TextureSizes.plateSize)
    ): Entity {
        val plate = Plate(
            position = position,
            dimensions = dimensions
        )
        EntityManager.registerEntity(plate)
        return plate
    }

    private fun createPan(position: Vector2D): Entity {
        val pan = Pan(position)
        EntityManager.registerEntity(pan)
        return pan
    }

    private fun createDeliverySection(platePosition: Vector2D) {
        val deliverySection =
            DeliverySection(
                position = platePosition
            )
        EntityManager.registerEntity(deliverySection)
        EntityManager.deliverySection = deliverySection
    }

    private fun createOven(position: Vector2D) {
        EntityManager.registerEntity(Oven(position))
    }

    private fun createWall(
        position: Vector2D,
        dimensions: Dimensions,
        pushDirection: Direction
    ): Wall {
        val wall = Wall(
            position = position,
            dimensions = dimensions,
            pushDirection = pushDirection
        )
        EntityManager.registerEntity(wall)
        return wall
    }

    private fun createWorldWalls(includeTop: Boolean = false) {
        createWall(
            position = Vector2D(0f, EntityManager.worldHeight - 10f),
            dimensions = Dimensions(EntityManager.worldWidth, EntityManager.worldHeight),
            pushDirection = Direction.UP
        )
        createWall(
            position = Vector2D(-EntityManager.worldWidth + 10f, 0f),
            dimensions = Dimensions(EntityManager.worldWidth, EntityManager.worldHeight),
            pushDirection = Direction.RIGHT
        )
        createWall(
            position = Vector2D(EntityManager.worldWidth - 10f, 0f),
            dimensions = Dimensions(EntityManager.worldWidth, EntityManager.worldHeight),
            pushDirection = Direction.LEFT
        )
        if (includeTop) {
            createWall(
                position = Vector2D(0f, -EntityManager.worldHeight + 10f),
                dimensions = Dimensions(EntityManager.worldWidth, EntityManager.worldHeight),
                pushDirection = Direction.DOWN
            )
        }
    }

    private fun createFridge(position: Vector2D) {

        val availableIngredients: List<Ingredient> = listOf(
            Tomato(
                position = Vector2D(
                    position.x - TextureSizes.fridgeSize * (6f / 16f),
                    position.y - TextureSizes.fridgeSize * (5f / 32f)
                )
            ),
            Egg(
                position = Vector2D(
                    position.x - TextureSizes.fridgeSize * (3f / 16f),
                    position.y - TextureSizes.fridgeSize * (5f / 32f)
                )
            ),
            Lettuce(
                position = Vector2D(
                    position.x + TextureSizes.fridgeSize * (0f / 16f),
                    position.y - TextureSizes.fridgeSize * (4f / 32f)
                )
            ),
            Bacon(
                position = Vector2D(
                    position.x + TextureSizes.fridgeSize * (3f / 16f),
                    position.y - TextureSizes.fridgeSize * (4f / 32f)
                )
            ),
            Cheese(
                position = Vector2D(
                    position.x - TextureSizes.fridgeSize * (6f / 16f),
                    position.y + TextureSizes.fridgeSize * (6f / 32f)
                )
            ),
            HamburgerBunTop(
                position = Vector2D(
                    position.x - TextureSizes.fridgeSize * (3f / 16f),
                    position.y + TextureSizes.fridgeSize * (6f / 32f)
                )
            ),
            HamburgerBunBottom(
                position = Vector2D(
                    position.x + TextureSizes.fridgeSize * (0f / 16f),
                    position.y + TextureSizes.fridgeSize * (6f / 32f)
                )
            ),
            Steak(
                position = Vector2D(
                    position.x + TextureSizes.fridgeSize * (3f / 16f),
                    position.y + TextureSizes.fridgeSize * (6f / 32f)
                )
            ),
            Patty(
                position = Vector2D(
                    position.x - TextureSizes.fridgeSize * (6f / 16f),
                    position.y + TextureSizes.fridgeSize * (19f / 32f)
                )
            )
        )

        val fridge = Fridge(
            position = position, ingredients = availableIngredients
        )

        EntityManager.registerEntity(fridge)
        EntityManager.spawnFridge = fridge
    }


    fun createDebugHitboxes(entity: Entity) {
        if (entity.physicalBody != null) {
            for (rect in entity.physicalBody!!.rectangleHitboxes) {
                val color =
                    if (entity is Wall) Color.DKGRAY else if (entity is Plate) Color.RED else Color.BLUE
                val hitbox = createDebugHitbox(rectangleHitbox = rect, color = color)
                entity.debugHitboxes.add(hitbox)
            }
            for (circle in entity.physicalBody!!.circleHitboxes) {
                val color =
                    if (entity is Wall) Color.DKGRAY else if (entity is Plate) Color.RED else Color.BLUE
                val hitbox = createDebugHitbox(circleHitbox = circle, color = color)
                entity.debugHitboxes.add(hitbox)
            }
        }
    }

    fun createDebugHitbox(
        circleHitbox: CircleHitbox? = null,
        rectangleHitbox: RectangleHitbox? = null,
        color: Int = Color.BLACK
    ): DebugHitbox {
        val hitbox = DebugHitbox(
            rectangleHitbox = rectangleHitbox,
            circleHitbox = circleHitbox,
            color = color
        )
        EntityManager.registerEntity(hitbox)
        return hitbox
    }

}