package com.wok_in_progress.physics

data class CircleHitbox(
    var position: Vector2D,
    var radius: Float,
    var dy: Float,
    var dx: Float,
    var rotation: Float = 0f
) {
    constructor(other: CircleHitbox) : this(
        position = Vector2D(other.position),
        radius = other.radius,
        dy = other.dy,
        dx = other.dx,
        rotation = other.rotation
    )

    fun updatePosition(x: Float, y: Float) {
        position.set(x, y)
    }
}