package com.wok_in_progress.physics

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.log
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min


class PhysicalBody(
    var position: Vector2D,
    var dimensions: Dimensions,
    var velocity: Vector2D = Vector2D(),
    var rectangleHitboxes: List<RectangleHitbox> = listOf(),
    var circleHitboxes: List<CircleHitbox> = listOf(),
    var mass: Float = 0f, // Grams, used for collisions
    val movable: Boolean = true, // Whether the body ever changes position
    val collidable: Boolean = true, // Whether doCollisions is applicable
    val bounciness: Float = 0.05f,
    val isContainer: Boolean = false, // Whether the body can contain other bodies
    val isWall: Boolean = false, // Whether the body will act as a wall and repel other bodies
    val pushDirection: Direction = Direction.UP, // Direction to push if body is a wall
    var collisions: HashSet<Entity> = hashSetOf(), // To avoid handling a collision two ways
    var isOnGround: Boolean = false, // Whether the body is on the ground and does not care about gravity
    var lastPosition: Vector2D = position,
    var isStatic: Boolean = false // Never updated
) {
    var top = position.y - dimensions.height / 2f
    var bottom = position.y + dimensions.height / 2f
    var left = position.x - dimensions.width / 2f
    var right = position.x + dimensions.width / 2f

    constructor(other: PhysicalBody) : this(
        position = other.position.copy(),
        dimensions = Dimensions(other.dimensions),
        velocity = other.velocity.copy(),
        rectangleHitboxes = other.rectangleHitboxes.map { RectangleHitbox(it) },
        circleHitboxes = other.circleHitboxes.map { CircleHitbox(it) },
        mass = other.mass,
        movable = other.movable,
        collidable = other.collidable,
        bounciness = other.bounciness,
        isWall = other.isWall,
        pushDirection = other.pushDirection,
        collisions = other.collisions.toHashSet(),
        isOnGround = other.isOnGround,
        lastPosition = other.lastPosition.copy()
    )

    /**
     * Updates position to given position. Preserves relative distance of hitboxes. If rotation
     * is implemented, relative distances need to account for angles as well.
     */
    fun updatePosition(newPosition: Vector2D) {
        top = newPosition.y
        bottom = newPosition.y
        left = newPosition.x
        right = newPosition.x
        for (circle in circleHitboxes) {
            val x = newPosition.x + circle.dx
            val y = newPosition.y + circle.dy
            circle.updatePosition(x, y)
            top = min(circle.position.y - circle.radius, top)
            bottom = max(circle.position.y + (circle.radius), bottom)
            left = min(circle.position.x - circle.radius, left)
            right = max(circle.position.x + circle.radius, right)
        }
        for (rect in rectangleHitboxes) {
            val x = newPosition.x + rect.dx
            val y = newPosition.y + rect.dy
            rect.updatePosition(x, y)
            top = min(rect.topLine.startPoint.y, top)
            bottom = max(rect.bottomLine.startPoint.y, bottom)
            left = min(rect.leftLine.startPoint.x, left)
            right = max(rect.rightLine.startPoint.x, right)
        }
        lastPosition = position
        position = newPosition
    }

    /**
     * Updates position based on current velocity
     */
    fun updatePosition() {
        if ((abs(velocity.x) > 0f || abs(velocity.y) > 0f) && movable) {
            val newPosition = position.plus(velocity)
            updatePosition(newPosition)
        }
    }

    /**
     * Updates velocity to given velocity, respecting movable status
     */
    fun updateVelocity(newVelocity: Vector2D) {
        if (movable && !isStatic) velocity.set(newVelocity)
    }
}