package com.wok_in_progress.physics

data class Line(var startPoint: Vector2D, var endPoint: Vector2D)