package com.wok_in_progress.physics

import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sign
import kotlin.math.sqrt

data class Vector2D(var x: Float = 0f, var y: Float = 0f) {
    constructor(other: Vector2D) : this(x = other.x, y = other.y)
    constructor(other: SynchronizedVector2D) : this(x = other.x, y = other.y)
    constructor(other: Pair<Float, Float>) : this(x = other.first, y = other.second)

    fun copy(): Vector2D {
        return Vector2D(x, y)
    }

    fun set(other: Vector2D) {
        x = other.x
        y = other.y
    }

    fun set(x: Float, y: Float) {
        this.x = x
        this.y = y
    }

    fun add(other: Vector2D) {
        x += other.x
        y += other.y
    }

    fun distance(other: Vector2D): Float {
        return sqrt((x - other.x).pow(2) + (y - other.y).pow(2))
    }

    fun distanceX(other: Vector2D): Float {
        return abs(x - other.x)
    }

    fun distanceY(other: Vector2D): Float {
        return abs(y - other.y)
    }

    fun directionX(other: Vector2D): Int {
        return sign(x - other.x).toInt()
    }

    fun directionY(other: Vector2D): Int {
        return sign(y - other.y).toInt()
    }

    fun norm(): Float {
        return sqrt(x.pow(2) + y.pow(2))
    }

    fun plus(other: Vector2D): Vector2D {
        return Vector2D(x = x + other.x, y = y + other.y)
    }

    fun minus(other: Vector2D): Vector2D {
        return Vector2D(x = x - other.x, y = y - other.y)
    }

    fun multiply(scalar: Float): Vector2D {
        return Vector2D(x * scalar, y * scalar)
    }

    fun div(scalar: Float): Vector2D {
        return Vector2D(x / scalar, y / scalar)
    }

    fun dot(other: Vector2D): Float {
        return x * other.x + y * other.y
    }

    fun scale(scalar: Float) {
        x *= scalar
        y *= scalar
    }

    fun scaleX(scalar: Float) {
        x *= scalar
    }

    fun scaleY(scalar: Float) {
        y *= scalar
    }

    fun flip() {
        scale(-1f)
    }

    fun flipX() {
        scaleX(-1f)
    }

    fun flipX(scalar: Float) {
        scaleX(-abs(scalar))
    }

    fun flipY() {
        scaleY(-1f)
    }

    fun flipY(scalar: Float) {
        scaleY(-abs(scalar))
    }

}