package com.wok_in_progress.physics

import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

fun didHitBody(point: Vector2D, body: PhysicalBody): Boolean {
    for (rect in body.rectangleHitboxes) {
        if (isPointInRectangle(point, rect)) {
            return true
        }
    }
    for (circle in body.circleHitboxes) {
        if (isPointInCircle(point, circle)) {
            return true
        }
    }
    return false
}

fun isOverlapHitboxesRect(body: PhysicalBody, rectangleHitbox: RectangleHitbox): Boolean {
    for (rect in body.rectangleHitboxes) {
        if (isOverlapRectangles(rect, rectangleHitbox)) return true
    }
    for (circle in body.circleHitboxes) {
        if (isOverlapCircleRectangle(circle, rectangleHitbox)) return true
    }
    return false
}

fun isOverlapHitboxesLine(body: PhysicalBody, line: Line): Boolean {
    for (rect in body.rectangleHitboxes) {
        if (doesLineIntersectRectangle(line, rect)) return true
    }
    for (circle in body.circleHitboxes) {
        val intersect = doesLineIntersectCircle(line, circle)
        if (intersect) return true
    }
    return false
}

fun isCollision(a: PhysicalBody, b: PhysicalBody): Boolean {
    for (aCircle in a.circleHitboxes) {
        for (bCircle in b.circleHitboxes) {
            if (isOverlapCircles(aCircle, bCircle)) return true
        }
        for (bRect in b.rectangleHitboxes) {
            if (isOverlapCircleRectangle(aCircle, bRect)) return true
        }
    }
    for (aRect in a.rectangleHitboxes) {
        for (bRect in b.rectangleHitboxes) {
            if (isOverlapRectangles(aRect, bRect)) return true
        }
        for (bCircle in b.circleHitboxes) {
            if (isOverlapCircleRectangle(bCircle, aRect)) return true
        }
    }
    return false
}

fun isOverlapRectangles(a: RectangleHitbox, b: RectangleHitbox): Boolean {
    return when {
        isPointInRectangle(a.a, b) -> true
        isPointInRectangle(a.b, b) -> true
        isPointInRectangle(a.c, b) -> true
        isPointInRectangle(a.d, b) -> true
        isPointInRectangle(b.a, a) -> true
        isPointInRectangle(b.b, a) -> true
        isPointInRectangle(b.c, a) -> true
        isPointInRectangle(b.d, a) -> true
        else -> false
    }
}

fun isOverlapCircles(a: CircleHitbox, b: CircleHitbox): Boolean {
    val distanceBetweenCentres =
        sqrt(
            (a.position.x - b.position.x).pow(2)
                    + (a.position.y - b.position.y).pow(2)
        )
    return distanceBetweenCentres <= a.radius + b.radius
}

fun isOverlapCircleRectangle(circle: CircleHitbox, rectangle: RectangleHitbox): Boolean {
    val pointA = rectangle.a
    val pointB = rectangle.b
    val pointC = rectangle.c
    val pointD = rectangle.d
    val ab = Line(pointA, pointB)
    val ad = Line(pointA, pointD)
    val bc = Line(pointB, pointC)
    val cd = Line(pointC, pointD)

    return when {
        doesLineIntersectCircle(ab, circle) -> true
        doesLineIntersectCircle(ad, circle) -> true
        doesLineIntersectCircle(bc, circle) -> true
        doesLineIntersectCircle(cd, circle) -> true
        isPointInRectangle(circle.position, rectangle) -> true
        isPointInCircle(rectangle.position, circle) -> true
        else -> false
    }
}

fun doesLineIntersectLine(lineA: Line, lineB: Line): Boolean {
    val a = lineA.startPoint.x
    val b = lineA.startPoint.y
    val c = lineA.endPoint.x
    val d = lineA.endPoint.y
    val p = lineB.startPoint.x
    val q = lineB.startPoint.y
    val r = lineB.endPoint.x
    val s = lineB.endPoint.y
    val det = (c - a) * (s - q) - (r - p) * (d - b)
    return if (det == 0f) {
        false
    } else {
        val lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det
        val gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det
        (0f < lambda && lambda < 1f) && (0f < gamma && gamma < 1f)
    }
}

fun doesLineIntersectRectangle(line: Line, rectangle: RectangleHitbox): Boolean {
    return when {
        doesLineIntersectLine(line, rectangle.topLine) -> true
        doesLineIntersectLine(line, rectangle.leftLine) -> true
        doesLineIntersectLine(line, rectangle.rightLine) -> true
        doesLineIntersectLine(line, rectangle.bottomLine) -> true
        else -> false
    }
}

fun doesLineIntersectCircle(line: Line, circle: CircleHitbox): Boolean {
    // Using this: https://stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm
    val r: Float = circle.radius
    val d: Vector2D = line.endPoint.minus(line.startPoint)
    val f: Vector2D = line.startPoint.minus(circle.position)
    val a: Float = d.dot(d)
    val b: Float = 2 * f.dot(d)
    val c: Float = f.dot(f) - r * r
    var discriminant: Float = b.pow(2) - 4 * a * c
    val intersect: Boolean
    if (discriminant < 0f) {
        intersect = false
    } else {
        discriminant = sqrt(discriminant)
        val t1: Float = (-b - discriminant) / (2f * a)
        val t2: Float = (-b + discriminant) / (2f * a)
        intersect = if (t1 in 0f..1f) true else t2 in 0f..1f
    }
    return intersect
}

fun isPointInRectangle(point: Vector2D, rectangle: RectangleHitbox): Boolean {
    // Using this: https://www.geeksforgeeks.org/check-whether-given-point-lies-inside-rectangle-not/
    val epsilon = 0.01f // Small value to compensate for rounding errors. Might need adjustment
    val areaRectangle = areaRectangle(rectangle)
    val abp = areaTriangle(rectangle.a, rectangle.b, point)
    val bcp = areaTriangle(rectangle.b, rectangle.c, point)
    val cdp = areaTriangle(rectangle.c, rectangle.d, point)
    val adp = areaTriangle(rectangle.a, rectangle.d, point)
    return (abp + bcp + cdp + adp) in ((areaRectangle - epsilon)..(areaRectangle + epsilon))
}

fun isPointInCircle(point: Vector2D, circle: CircleHitbox): Boolean {
    val x1 = point.x - circle.position.x
    val y1 = point.y - circle.position.y
    return (x1.pow(2) + y1.pow(2)) <= circle.radius.pow(2)
}

fun areaPolygon(x: List<Float>, y: List<Float>, n: Int): Float {
    // Using this: https://www.geeksforgeeks.org/c-program-find-area-triangle/
    var area = 0f
    var j = n - 1
    for (i in 0 until n) {
        area += (x[j] + x[i]) * (y[j] - y[i])
        j = i
    }
    return abs(area / 2f)
}

fun areaRectangle(rectangle: RectangleHitbox): Float {
    val x = listOf(rectangle.a.x, rectangle.b.x, rectangle.c.x, rectangle.d.x)
    val y = listOf(rectangle.a.y, rectangle.b.y, rectangle.c.y, rectangle.d.y)
    return areaPolygon(x, y, 4)
}

fun areaTriangle(p1: Vector2D, p2: Vector2D, p3: Vector2D): Float {
    // Finding the lengths of the sides first
    val x = listOf(p1.x, p2.x, p3.x)
    val y = listOf(p1.y, p2.y, p3.y)
    return areaPolygon(x, y, 3)
}