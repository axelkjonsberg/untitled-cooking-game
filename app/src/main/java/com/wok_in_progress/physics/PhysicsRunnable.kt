package com.wok_in_progress.physics

import android.os.Handler
import com.wok_in_progress.log
import com.wok_in_progress.rules.Game
import com.wok_in_progress.rules.GameState
import kotlin.math.floor
import kotlin.system.measureTimeMillis

class PhysicsRunnable(val handler: Handler) : Runnable {
    private var paused = false
    private var running = false
    private val targetTime = 14f
    var averageTime = 0f
    var totalTime = 0f
    var timesRun = 0

    init {
        Game.subscribe(GameState.Running, ::resume)
        Game.subscribe(GameState.Paused, ::pause)
    }

    private fun resume() {
        paused = false
        handler.postDelayed(this, 0)
    }

    private fun pause() {
        paused = true
    }

    override fun run() {
        if (!running && !paused && Game.gameState == GameState.Running) {
            running = true
            var timeVelocities: Long = 0
            var timePositions: Long = 0
            val time = measureTimeMillis {
                if (PhysicsEngine.zonesWithBodies.isEmpty()) {
                    val zoneTime = measureTimeMillis {
                        PhysicsEngine.createZones()
                    }
                    log("PhysicsRunnable: Creating zones time: $zoneTime ms")
                }
                timeVelocities = measureTimeMillis {
                    PhysicsEngine.doCollisions()
                }
                timePositions = measureTimeMillis {
                    PhysicsEngine.update()
                }
            }
            timesRun += 1
            totalTime += time
            averageTime = totalTime / timesRun
            if (timesRun % 1000 == 0 || timesRun == 1) {
                log("PhysicsRunnable: Physics average time: $averageTime ms")
                log("PhysicsRunnable: Collisions: $timeVelocities, updates: $timePositions")
            }
            running = false
            if (time > targetTime) {
                handler.postDelayed(this, 0)
            } else {
                handler.postDelayed(this, (targetTime - time).toLong())
            }
        } else log("PhysicsRunnable: Decided not to run")
    }
}
