package com.wok_in_progress.physics

data class Dimensions(var width: Float, var height: Float) {
    constructor(other: Dimensions) : this(width = other.width, height = other.height)
    constructor(size: Float) : this(width = size, height = size)

    fun update(width: Float, height: Float) {
        this.width = width
        this.height = height
    }
}