package com.wok_in_progress.physics

data class RectangleHitbox(
    var position: Vector2D,
    var dimensions: Dimensions,
    var dx: Float,
    var dy: Float,
    var a: Vector2D = Vector2D(position.x, position.y),
    var b: Vector2D = Vector2D(position.x, position.y + dimensions.height),
    var c: Vector2D = Vector2D(position.x + dimensions.width, position.y + dimensions.height),
    var d: Vector2D = Vector2D(position.x + dimensions.width, position.y),
    var rotation: Float = 0f
) {
    val topLine = Line(a, d)
    val leftLine = Line(a, b)
    val rightLine = Line(d, c)
    val bottomLine = Line(b, c)

    constructor(other: RectangleHitbox) : this(
        position = Vector2D(other.position),
        dimensions = Dimensions(other.dimensions),
        dx = other.dx,
        dy = other.dy
    )

    fun updatePosition(x: Float, y: Float) {
        position.set(x, y)
        updatePoints()
    }

    fun updatePoints() {
        a.set(position.x, position.y)
        b.set(position.x, position.y + dimensions.height)
        c.set(position.x + dimensions.width, position.y + dimensions.height)
        d.set(position.x + dimensions.width, position.y)
    }
}