package com.wok_in_progress.physics

class SynchronizedVector2D(x: Float, y: Float) {
    var vector = Vector2D(x, y)
    var x: Float
        @Synchronized
        get() = this.get().x
        @Synchronized
        set(value) {
            this.vector.x = value
        }
    var y: Float
        @Synchronized
        get() = this.get().y
        @Synchronized
        set(value) {
            this.vector.y = value
        }

    @Synchronized
    fun get(): Vector2D {
        return vector
    }

    @Synchronized
    fun set(vector: Vector2D) {
        this.vector.set(vector)
    }

    @Synchronized
    fun add(vector: Vector2D) {
        this.vector.add(vector)
    }

    @Synchronized
    fun flipX() {
        this.vector.flipX()
    }

    @Synchronized
    fun flipY() {
        this.vector.flipY()
    }


}