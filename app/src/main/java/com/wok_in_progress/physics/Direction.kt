package com.wok_in_progress.physics

enum class Direction {
    UP, DOWN, LEFT, RIGHT
}