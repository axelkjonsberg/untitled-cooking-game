package com.wok_in_progress.physics

import com.wok_in_progress.entities.Entity
import com.wok_in_progress.entities.layout_entities.Wall
import com.wok_in_progress.interaction.InteractionManager
import com.wok_in_progress.log
import com.wok_in_progress.error
import kotlin.math.*

object PhysicsEngine {
    // The Physics Engine handles collisions and velocities of objects
    private val registeredEntities = mutableListOf<Entity>()

    private val gravity = Vector2D(y = 0f)
    private var groundFriction = 0.8f

    private var doGravity = true
    private var doCollisions = true
    private var useVelocity = true
    var debug = false

    private const val numZoneDivisions = 16
    var zonesWithBodies: MutableList<MutableList<MutableList<Entity>>> = mutableListOf()
    var bodiesToZones: HashMap<Entity, LinkedHashSet<Pair<Int, Int>>> = hashMapOf()

    private var worldWidth: Float = 0f
    private var worldHeight: Float = 0f

    fun setWorldDimensions(width: Float, height: Float) {
        worldWidth = width
        worldHeight = height
        gravity.y = 1200f / worldHeight // To get the same behaviour on different screens
    }

    fun createZones() {
        val numLines: Int = numZoneDivisions
        // Ensuring no leftovers in lists if run twice
        zonesWithBodies = mutableListOf()
        bodiesToZones = hashMapOf()
        for (i in 0 until numLines) {
            zonesWithBodies.add(mutableListOf())
            for (j in 0 until numLines) {
                zonesWithBodies[i].add(mutableListOf())
            }
        }
    }

    fun doCollisions() {
        // TODO: If zones aren't enough, use one thread per zone. Consider coroutines
        //  with launch keyword and not global scope for lightweight threading
        for (col in zonesWithBodies) {
            for (zone in col) {
                for (entity1 in zone) {
                    if (doCollisions && zone.size > 1) {
                        val bodyA = entity1.physicalBody!!
                        if (bodyA.isStatic || !bodyA.collidable) continue
                        for (index2 in (0 until zone.size)) {
                            // TODO: Use continuous collision to avoid "missing" a collision.
                            //  Speculative cc would work just fine.
                            val entity2 = zone[index2]
                            val bodyB = entity2.physicalBody!!
                            if (!bodyB.collidable) continue
                            val isCollision = isCollision(bodyA, bodyB)
                            if (isCollision) {
                                // Must avoid handling a collision more than once, since it may happen
                                // hundreds of times in a row
                                if (!bodyA.collisions.contains(entity2)) InteractionManager.interact(
                                    entity1,
                                    entity2
                                )
                                if (bodyA.collidable && bodyB.collidable) {
                                    if (bodyA.movable && bodyB.movable && !(bodyA.isContainer || bodyB.isContainer)) {
                                        handleCollision(bodyA, bodyB, entity1, entity2)
                                    } else if (bodyA.movable && (bodyB.isContainer || bodyB.isWall)) handleBounce(
                                        entity1,
                                        entity2
                                    )
                                    else if ((bodyA.isContainer || bodyA.isWall) && bodyB.movable) handleBounce(
                                        entity2,
                                        entity1
                                    )
                                }

                            } else InteractionManager.notInteract(entity1, entity2)
                            if (!isCollision) {
                                bodyA.collisions.remove(entity2)
                                bodyB.collisions.remove(entity1)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun horizontalIndex(x: Float): Int {
        return floor(x / (worldWidth / numZoneDivisions)).toInt()
    }

    private fun verticalIndex(y: Float): Int {
        return floor(y / (worldHeight / numZoneDivisions)).toInt()
    }

    var timesRun = 0
    fun update() {
        for (entity in registeredEntities) {
            val body = entity.physicalBody ?: continue
            if (bodiesToZones[entity] == null) bodiesToZones[entity] = linkedSetOf()
            if (body.isStatic && bodiesToZones[entity]!!.toList().isNotEmpty()) continue
            if (body.isOnGround && !body.collisions.any { it is Wall || it.physicalBody?.isContainer == true }) body.isOnGround =
                false
            // Use velocity and acceleration
            if (doGravity && !body.isOnGround && !body.isStatic) body.updateVelocity(
                body.velocity.plus(
                    gravity
                )
            )
            if (useVelocity) entity.updatePosition()
            // Update what zones the body is in
            val inhabitingZones = bodiesToZones[entity]!!.toMutableList()
            val horizontalIndexes = horizontalIndex(body.left)..horizontalIndex(body.right)
            val verticalIndexes = verticalIndex(body.top)..verticalIndex(body.bottom)
            val pointList = mutableListOf<Pair<Int, Int>>()
            for (i in horizontalIndexes) {
                for (j in verticalIndexes) {
                    pointList.add(Pair(i, j))
                }
            }
            // Check if no longer in current zone
            for (zone in inhabitingZones) {
                var inside = false
                if (zone in pointList) inside = true
                if (!inside) {
                    zonesWithBodies[zone.first][zone.second].remove(entity)
                    bodiesToZones[entity]!!.remove(zone)
                }
            }
            // Find new zones
            for (point in pointList) {
                if (point.first in 0 until numZoneDivisions && point.second in 0 until numZoneDivisions) {
                    val inZonesWithBodies =
                        zonesWithBodies[point.first][point.second].contains(entity)
                    if (!inZonesWithBodies) {
                        zonesWithBodies[point.first][point.second].add(entity)
                        bodiesToZones[entity]!!.add(point)
                    }
                }
            }
        }
        timesRun++
    }

    fun registerEntity(entity: Entity) {
        if (entity in registeredEntities) error("PhysicsEngine: Duplicate entity attempted registered!")
        else {
            registeredEntities.add(entity)
            bodiesToZones[entity] = linkedSetOf()
        }
    }

    fun unregisterEntity(entity: Entity) {
        if (entity in registeredEntities) registeredEntities.remove(entity)
        if (entity in bodiesToZones.keys) bodiesToZones.remove(entity)
        zonesWithBodies.forEach { it ->
            it.forEach {
                if (entity in it) it.remove(entity)
            }
        }
    }

    private fun handleCollision(
        a: PhysicalBody,
        b: PhysicalBody,
        entityA: Entity,
        entityB: Entity
    ) {
        if (!a.collisions.contains(entityB)) {
            a.collisions.add(entityB)
            b.collisions.add(entityA)
            // Using this: https://www.emanueleferonato.com/2007/08/19/managing-ball-vs-ball-collision-with-flash/
            val mA = a.mass
            val mB = b.mass
            val dx = a.position.x - b.position.x
            val dy = a.position.y - b.position.y
            val collisionAngle = atan2(dy, dx)
            val aMagnitude = a.velocity.norm()// sqrt(a.velocity.x.pow(2) + a.velocity.y.pow(2))
            val bMagnitude = b.velocity.norm()// sqrt(b.velocity.x.pow(2) + b.velocity.y.pow(2))
            val aDirection = atan2(a.velocity.y, a.velocity.x)
            val bDirection = atan2(b.velocity.y, b.velocity.x)
            val aTransVx = aMagnitude * cos(aDirection - collisionAngle)
            val aTransVy = aMagnitude * sin(aDirection - collisionAngle)
            val bTransVx = bMagnitude * cos(bDirection - collisionAngle)
            val bTransVy = bMagnitude * sin(bDirection - collisionAngle)
            val aTransVxf = ((mA - mB) * aTransVx + (mB + mB) * bTransVx) / (mA + mB)
            val bTransVxf = ((mA + mA) * aTransVx + (mB - mA) * bTransVx) / (mA + mB)
            val aNewVx = cos(collisionAngle) * aTransVxf + cos(collisionAngle + PI / 2) * aTransVy
            val aNewVy = sin(collisionAngle) * aTransVxf + sin(collisionAngle + PI / 2) * aTransVy
            val bNewVx = cos(collisionAngle) * bTransVxf + cos(collisionAngle + PI / 2) * bTransVy
            val bNewVy = sin(collisionAngle) * bTransVxf + sin(collisionAngle + PI / 2) * bTransVy
            a.updateVelocity(Vector2D(aNewVx.toFloat(), aNewVy.toFloat()))
            b.updateVelocity(Vector2D(bNewVx.toFloat(), bNewVy.toFloat()))
        } else {
            // TODO: Push objects away from each other at a set rate until they do not touch
        }

    }

    private fun handleBounce(moving: Entity, static: Entity) {
        val staticBody = static.physicalBody!!
        val movingBody = moving.physicalBody!!
        movingBody.collisions.add(static)
        val correctionFactor = 0.5f
        if (staticBody.rectangleHitboxes.isNotEmpty()) {
            val staticHitbox = staticBody.rectangleHitboxes.first()
            if (!staticBody.isWall) {
                val bottomLine = staticHitbox.bottomLine
                if (isOverlapHitboxesLine(movingBody, bottomLine)) {
                    if (movingBody.velocity.y > 0f) {
                        movingBody.updateVelocity(
                            Vector2D(
                                movingBody.velocity.x
                            )
                        )
                    }
                    val distanceBottom =
                        bottomLine.startPoint.distanceY(Vector2D(y = movingBody.bottom))
                    movingBody.updatePosition(
                        Vector2D(
                            movingBody.position.x,
                            movingBody.position.y - (distanceBottom) * correctionFactor
                        )
                    )
                    movingBody.velocity.scaleX(groundFriction)
                    movingBody.isOnGround = true
                } else {
                    movingBody.isOnGround = false
                }
            } else if (staticBody.isWall) {
                var distance = when (staticBody.pushDirection) {
                    Direction.UP -> staticHitbox.a.distanceY(Vector2D(y = movingBody.bottom))
                    Direction.DOWN -> staticHitbox.b.distanceY(Vector2D(y = movingBody.top))
                    Direction.LEFT -> staticHitbox.a.distanceX(Vector2D(x = movingBody.right))
                    Direction.RIGHT -> staticHitbox.d.distanceX(Vector2D(x = movingBody.left))
                }
                if ((staticBody.pushDirection == Direction.UP || staticBody.pushDirection == Direction.DOWN)) {
                    if (movingBody.velocity.y > 0f) {
                        movingBody.updateVelocity(
                            Vector2D(
                                movingBody.velocity.x
                            )
                        )
                    }
                    distance = if (staticBody.pushDirection == Direction.UP) distance else -distance
                    movingBody.updatePosition(
                        Vector2D(
                            movingBody.position.x,
                            movingBody.position.y - (distance) * correctionFactor
                        )
                    )
                    if (staticBody.pushDirection == Direction.UP) movingBody.velocity.scaleX(
                        groundFriction
                    )
                    movingBody.isOnGround = staticBody.pushDirection == Direction.UP
                } else if ((staticBody.pushDirection == Direction.LEFT || staticBody.pushDirection == Direction.RIGHT)) {
                    if (movingBody.velocity.x > 0f) {
                        movingBody.velocity.flipX(movingBody.bounciness)
                    }
                    distance =
                        if (staticBody.pushDirection == Direction.LEFT) distance else -distance
                    movingBody.updatePosition(
                        Vector2D(
                            movingBody.position.x - (distance) * correctionFactor,
                            movingBody.position.y
                        )
                    )
                }
            }
        }
    }

    fun selectObject(location: Pair<Float, Float>): Entity? {
        val locationVector = Vector2D(location)
        var closest: Pair<Entity?, Float> = Pair(null, Float.MAX_VALUE)
        for (entity in registeredEntities) {
            val body = entity.physicalBody!!
            if (didHitBody(locationVector, body)) {
                val distance = body.position.distance(locationVector)
                if (distance < closest.second) {
                    closest = Pair(entity, distance)
                }
            }
        }
        return closest.first
    }
}
