package com.wok_in_progress.interaction

import android.graphics.Color
import android.view.MotionEvent
import com.wok_in_progress.entities.Entity
import com.wok_in_progress.entities.EntityCreator
import com.wok_in_progress.entities.EntityManager
import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.entities.layout_entities.Button
import com.wok_in_progress.entities.layout_entities.DeliverySection
import com.wok_in_progress.entities.layout_entities.Fridge
import com.wok_in_progress.log
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.PhysicsEngine
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.physics.didHitBody
import com.wok_in_progress.rendering.Renderer
import com.wok_in_progress.rendering.renderables.Sprite
import com.wok_in_progress.rules.Game
import com.wok_in_progress.rules.GameManager
import com.wok_in_progress.rules.GameState
import com.wok_in_progress.rules.RuleManager
import kotlin.math.floor

object InteractionManager {
    // Touch the screen, and I figure out which entity to select and affect. I also apply
    // the speed of the finger to an object when it is released. Flick that tomato!

    private val fingers: LinkedHashMap<Int, Pair<Float, Float>?> = linkedMapOf()
    private val fingersHoldingObjects: LinkedHashMap<Int, Entity> = linkedMapOf()
    private var numFingers = 0
    private var selectedSection = -1
    private const val numberOfSections = 4
    private var worldWidth = 0f
    private var worldHeight = 0f
    private var backButton: Entity? = null
    private var deliveryButton: Entity? = null
    var buttons: MutableList<Button> = mutableListOf()
    var plate: Entity? = null
    var bigPlate: Entity? = null

    fun handleTouch(event: MotionEvent?) {
        if (event != null) {
            // MotionEvent { action=ACTION_DOWN, actionButton=0, id[0]=0, x[0]=470.0, y[0]=1158.0, toolType[0]=TOOL_TYPE_FINGER, buttonState=0, classification=NONE, metaState=0, flags=0x0, edgeFlags=0x0, pointerCount=1, historySize=0, eventTime=149358962, downTime=149358962, deviceId=6, source=0x1002, displayId=0 }
            val action = event.action
            val stringAction = MotionEvent.actionToString(action)
            val pointerCount = event.pointerCount
            //val eventTime = event.eventTime
            //val downTime = event.downTime
            if (action == 3) {
                log("InteractionManager: Cancel action happened")
                return
            }
            val fingerIndex = getFingerIndex(action)
            val multiFingerCheck = (action - 261) % 256
            if (pointerCount < numFingers) {
                log("InteractionManager: Fewer fingers")
            }
            if ((action == 0 || multiFingerCheck == 0) && fingerIndex != -1) {
                setFingerPosition(fingerIndex, event)
                numFingers += 1
                handleNewFinger(fingerIndex)
            } else if ((action == 1 || action == 6 || multiFingerCheck == 1) && fingerIndex != -1) {
                //log("InteractionManager: Attempting to remove finger $fingerIndex")
                handleRemovedFinger(fingerIndex)
                fingers.remove(fingerIndex)
                numFingers -= 1
            } else if (action == 2) {
                for (i in 0 until pointerCount) {
                    setFingerPosition(i, event)
                    updateHeldObjectPosition(i)
                }
            } else if (pointerCount == 0) {
                for (index in fingersHoldingObjects.keys) {
                    handleRemovedFinger(index)
                }
            } else {
                log("InteractionManager: A touch event was unaccounted for: $action, $stringAction")
            }
        }

    }

    fun setWorldDimensions(width: Float, height: Float) {
        worldWidth = width
        worldHeight = height
    }

    private fun setFingerPosition(index: Int, event: MotionEvent) {
        val correctedPosition = transformTouchPoint(Vector2D(event.getX(index), event.getY(index)))
        fingers[index] = if (selectedSection == -1) {
            Pair(event.getX(index), event.getY(index))
        } else {
            Pair(
                correctedPosition.x,
                correctedPosition.y
            )
        }
    }

    private fun getFingerIndex(action: Int): Int {
        // ACTION_DOWN = 0
        // ACTION_MOVE = 2
        // ACTION_UP = 1
        // ACTION_POINTER_UP(0) = 6
        // ACTION_POINTER_DOWN(1) = 261
        // ACTION_POINTER_UP(1) = 262
        // ACTION_POINTER_DOWN(2) = 517
        // ACTION_POINTER_UP(2) = 518
        // ACTION_POINTER_DOWN(3) = 773
        // ACTION_POINTER_UP(3) = 774
        // ACTION_POINTER_UP(9) = 2310
        if (action == 0 || action == 1 || action == 6) return 0
        val check = (action - 261) % 256
        if (check == 0) {
            return ((action - 261) / 256) + 1
        } else if (check == 1) {
            return ((action - 262) / 256) + 1
        }
        return -1
    }

    private fun updateHeldObjectPosition(index: Int) {
        val fingerPosition = Vector2D(fingers[index]!!)
        fingersHoldingObjects[index]?.updatePosition(fingerPosition)
    }

    private fun updateHeldObjectVelocity(index: Int) {
        val heldEntity = fingersHoldingObjects[index]
        if (heldEntity != null) {
            val body = heldEntity.physicalBody
            if (body != null) {
                val lastPosition = body.lastPosition
                val currentPosition = body.position
                val dx = currentPosition.x - lastPosition.x
                val dy = currentPosition.y - lastPosition.y
                val estimatedFingerVelocity = Vector2D(dx, dy)
                body.updateVelocity(estimatedFingerVelocity)
            }
        }
    }

    private fun transformTouchPoint(position: Vector2D): Vector2D {
        val i = selectedSection - 1
        val cx = worldWidth / 2f
        val cy = worldHeight / 2f
        val dx = position.x - cx
        val dy = position.y - cy
        val newCX = i * (worldWidth / numberOfSections) + (worldWidth / (2f * numberOfSections))
        val newCY = 1f * (worldHeight / numberOfSections) + (worldHeight / (2f * numberOfSections))
        return Vector2D(newCX + (dx / numberOfSections), newCY + (dy / numberOfSections))
    }

    private fun getCenterXOfActiveSection(): Float {
        val i = 1 + (selectedSection - 1) * 2
        return worldWidth * (i.toFloat() / 8f)
    }

    private fun projectPosition(
        position: Vector2D,
        fromCenter: Vector2D,
        toCenter: Vector2D
    ): Vector2D {
        val dx = position.x - fromCenter.x
        val dy = position.y - fromCenter.y
        val newCX = dx + toCenter.x
        val newCY = dy + toCenter.y
        return Vector2D(newCX, newCY)
    }

    private fun handleSelectedSection(index: Int) {
        if (Game.gameState != GameState.Running) return
        val x = fingers[index]!!.first
        val y = fingers[index]!!.second
        val touchedSectionX = floor(x / (worldWidth / numberOfSections)).toInt() + 1
        val touchedSectionY = floor(y / (worldHeight / numberOfSections)).toInt() + 1
        if (touchedSectionY != 2) return
        selectedSection = touchedSectionX
        backButton = EntityCreator.createButton(
            position = transformTouchPoint(Vector2D()),
            dimensions = Dimensions(width = worldWidth * 0.055f, height = worldHeight * 0.015f),
            textValue = "Back",
            color = Color.parseColor("#616161"),
            callback = ::handleUnselectSection
        )
        if (selectedSection == 1) {
            deliveryButton = EntityCreator.createButton(
                position = transformTouchPoint(Vector2D(worldWidth * (0.6f), worldHeight * (0.1f))),
                dimensions = Dimensions(width = worldWidth * 0.08f, height = worldHeight * 0.02f),
                textValue = "Deliver",
                color = Color.parseColor("#303F9F"),
                callback = GameManager::endRound
            )
        }
        if (plate != null) {
            plate?.interactable?.contains?.forEach { entity ->
                val previousPosition =
                    if (entity.renderable != null) entity.renderable!!.position.copy() else if (entity.physicalBody != null) entity.physicalBody!!.position.copy() else null
                if (previousPosition != null) entity.updatePosition(
                    projectPosition(
                        position = previousPosition,
                        fromCenter = plate!!.renderable!!.position,
                        toCenter = Vector2D(
                            x = getCenterXOfActiveSection(),
                            y = plate!!.renderable!!.position.y
                        )
                    )
                )
            }
            plate?.updatePosition(
                Vector2D(
                    x = getCenterXOfActiveSection(),
                    y = plate!!.renderable!!.position.y
                )
            )
            Renderer.registerRenderable(plate?.renderable)
        }
        Renderer.updateSelectedSection(selectedSection)
    }

    fun handleUnselectSection() {
        selectedSection = -1
        EntityManager.unregisterEntity(backButton)
        backButton = null
        EntityManager.unregisterEntity(deliveryButton)
        deliveryButton = null
        Renderer.updateSelectedSection(selectedSection)
    }

    private fun selectButton(index: Int): Entity? {
        if (backButton != null && backButton?.physicalBody != null) {
            if (didHitBody(Vector2D(fingers[index]!!), backButton!!.physicalBody!!)) {
                return backButton!!
            }
        }
        return null
    }

    private fun handleNewFinger(index: Int) {
        log("Handling new finger at position ${fingers[index]!!}")
        val selectedButton = selectButton(index)
        if (selectedButton != null && selectedButton is Button) {
            selectedButton.interactable?.callback?.invoke()
            return
        }
        val selectedEntity = PhysicsEngine.selectObject(fingers[index]!!)
        if (selectedSection == -1) {
            handleSelectedSection(index)
        } else if (selectedEntity is Button) {
            selectedEntity.interactable?.callback?.invoke()
        } else if (selectedEntity != null) {
            if (selectedEntity.interactable != null && !selectedEntity.interactable!!.held && selectedEntity.interactable!!.movable) {
                // check if element is static
                if (selectedEntity.physicalBody!!.isStatic && selectedEntity is Ingredient) {
                    EntityManager.spawnFridge?.removeEntityFromFridge(selectedEntity)
                }
                selectedEntity.updateHeld(index)
                fingersHoldingObjects[index] = selectedEntity
                updateHeldObjectPosition(index)
                //log("Finger $index started holding object $selectedEntity")
            } else {
                // Check if fridge
                if (selectedEntity is Fridge) {
                    selectedEntity.toggleOpen()
                }
                //log("Finger $index found object $selectedEntity, but it was not interactable or was held by another")
            }
        } else {
            //log("Finger $index found object $selectedEntity, but it did not exist")
        }
    }

    private fun handleRemovedFinger(index: Int) {
        val heldEntity: Entity? = fingersHoldingObjects[index]
        if (heldEntity != null) {
            // First, deal with velocity
            updateHeldObjectVelocity(index)
            // Second, remove relation
            heldEntity.removeHeld()
            fingersHoldingObjects.remove(index)
            //log("Finger $index let go of object $heldEntity")
        } else {
            //log("No object was held")
        }
    }

    /**
     * General function for all interactions between entities (except collisions).
     * Mainly meant to be used for preparing ingredients by cutting, cooking cleaning or similar.
     * Also used for inserting entities into containers.
     * If other uses seem appropriate, go ahead and add them here.
     */
    fun interact(a: Entity, b: Entity) {
        if (a.interactable != null && b.interactable != null) {
            // Cutting
            val cuttableEntity =
                if (a.interactable!!.cuttable) a else if (b.interactable!!.cuttable) b else null
            if (cuttableEntity != null) {
                val cuttingEntity =
                    if (a.interactable!!.cuts) a else if (b.interactable!!.cuts) b else null
                if (cuttingEntity != null && cuttableEntity is Ingredient) {
                    cuttableEntity.cut()
                }
            }
            // Containing
            if (a.interactable!!.contains != null
                && b.interactable!!.contains == null
                && a.renderable is Sprite
                && b.renderable is Sprite
            ) {
                addToContents(a, b)
            } else if (a.interactable!!.contains == null
                && b.interactable!!.contains != null
                && a.renderable is Sprite
                && b.renderable is Sprite
            ) {
                addToContents(b, a)
            }
            // Cooking
            val cookableEntity =
                if (a.interactable!!.cookable) a else if (b.interactable!!.cookable) b else null
            if (cookableEntity != null) {
                val cookingEntity =
                    if (a.interactable!!.cooks) a else if (b.interactable!!.cooks) b else null
                if (cookingEntity != null && cookableEntity is Ingredient) {
                    cookableEntity.cook()
                }
            }
        }
    }

    fun notInteract(a: Entity, b: Entity) {
        removeFromContents(a, b)
        removeFromContents(b, a)
    }

    private fun addToContents(container: Entity, content: Entity) {
        if (container.interactable?.contains != null
            && !container.interactable!!.contains!!.contains(content)
        ) {
            container.interactable?.contains?.add(content)
            if (container is DeliverySection) log("${container.interactable?.contains}")
        }
    }

    private fun removeFromContents(container: Entity, content: Entity) {
        if (container.interactable?.contains != null
            && container.interactable!!.contains!!.contains(content)
        ) {
            container.interactable?.contains?.remove(content)
        }
    }
}