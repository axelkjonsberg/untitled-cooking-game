package com.wok_in_progress.interaction

import com.wok_in_progress.entities.Entity

class Interactable(
    val callback: (() -> Unit)? = null,
    val movable: Boolean = true,
    val cuts: Boolean = false,
    val cuttable: Boolean = false,
    val cookable: Boolean = false,
    val cooks: Boolean = false,
    val contains: MutableList<Entity>? = null,
    var held: Boolean = false,
    var heldBy: Int? = null
) {
    constructor(other: Interactable) : this(
        callback = other.callback,
        movable = other.movable,
        cuts = other.cuts,
        cuttable = other.cuttable,
        cookable = other.cookable,
        cooks = other.cooks,
        contains = other.contains?.toMutableList(),
        held = false,
        heldBy = null
    )

    fun reset() {
        contains?.clear()
    }

}