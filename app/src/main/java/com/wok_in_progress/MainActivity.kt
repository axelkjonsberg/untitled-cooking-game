package com.wok_in_progress

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.wok_in_progress.rules.Game

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun startGame(view: View) {
        val intent = Intent(this, GameActivity::class.java)
        Game.reset()
        startActivity(intent)
    }

    fun goToScoreboard(view: View) {
        val intent = Intent(this, ScoreboardActivity::class.java)
        startActivity(intent)
    }
}
