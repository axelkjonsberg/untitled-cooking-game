package com.wok_in_progress

import android.util.Log
import com.wok_in_progress.entities.EntityManager
import com.wok_in_progress.interaction.InteractionManager
import com.wok_in_progress.physics.PhysicsEngine
import com.wok_in_progress.rendering.Renderer
import com.wok_in_progress.rendering.TextureSizes

fun log(message: String) {
    Log.d("wokk", message)
}

fun error(message: String) {
    Log.e("wokk", message)
}

fun setWorldDimensions(width: Float, height: Float) {
    TextureSizes.setScreenDependentSizes(width, height)
    EntityManager.setWorldDimensions(width, height)
    PhysicsEngine.setWorldDimensions(width, height)
    Renderer.setWorldDimensions(width, height)
    InteractionManager.setWorldDimensions(width, height)
}

fun stringHasContent(string: String): Boolean {
    return string.trim().isNotEmpty()
}

const val versionNumber = 0.1F