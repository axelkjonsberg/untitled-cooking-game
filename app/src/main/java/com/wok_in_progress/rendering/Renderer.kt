package com.wok_in_progress.rendering

import android.content.Context
import android.graphics.*
import androidx.core.content.ContextCompat
import com.wok_in_progress.R
import com.wok_in_progress.entities.Entity
import com.wok_in_progress.log
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rendering.renderables.*

/* Yo, I am the renderer, and I render all the things.
I only render renderables, but Sprites are renderables too.
Most of the renderables will probably be Sprites anyways.
Text will probably also need to be a thing. If vector graphics are
going to be used, we will need to define classes for each
VectorRenderable or some other fitting name.
By the way, I am a singleton, so don't worry about whether I am the
correct instance or not. It's all the same.
*/

object Renderer {
    /* Renderables are put into layers. The background should be in layer 0.
       It's probably a good idea to define where the other objects should be.
       I suggest putting zone-like things (the plate, cutting board, pan) into layer 1.
       Let's call layer 1 "zones" for now.
       The moving objects can probably be in layer 2. They are supposed to collide anyways,
       so we don't need to worry about overlap. Let's call layer 2 "objects"
       Score and ui-stuff (whatever it will be) can be in layer 3. Let's call it "overlay".
    */
    private var layers: MutableList<MutableList<Renderable>> = mutableListOf()
    private val paint: Paint = Paint()
    var debug = false
    private var selectedSection = -1
    const val numberOfSections = 4
    var worldWidth = 0f
    var worldHeight = 0f
    private var scaleX: Float = 1f
    private var scaleY: Float = 1f
    private var projectionCenter: Vector2D = Vector2D(worldWidth / 2f, worldHeight / 2f)
    private var worldCenter: Vector2D = Vector2D(worldWidth / 2f, worldHeight / 2f)

    fun drawCall(canvas: Canvas, context: Context) {
        // Clear the screen and draw everything in the order of the layers.
        clearScreen(canvas, context)
        layers.forEach { layer ->
            layer.forEach { renderable ->
                drawRenderable(canvas, renderable)
            }
        }
    }

    fun setWorldDimensions(width: Float, height: Float) {
        worldWidth = width
        worldHeight = height
        worldCenter = Vector2D(worldWidth / 2f, worldHeight / 2f)
        projectionCenter = Vector2D(worldCenter)
    }

    fun updateSelectedSection(selectedSection: Int) {
        scaleX = if (selectedSection != -1) numberOfSections.toFloat() else 1f
        scaleY = if (selectedSection != -1) numberOfSections.toFloat() else 1f
        projectionCenter = if (selectedSection != -1) {
            val cx =
                (selectedSection - 1) * (worldWidth / numberOfSections) + (worldWidth / (2f * numberOfSections))
            val cy = 1f * (worldHeight / numberOfSections) + (worldHeight / (2f * numberOfSections))
            Vector2D(cx, cy)
        } else Vector2D(worldCenter)
        this.selectedSection = selectedSection
    }

    fun registerRenderable(renderable: Renderable?) {
        if (renderable == null) return
        val layer = renderable.layer
        while (layers.size < layer + 1) {
            layers.add(mutableListOf())
        }
        if (renderable !in layers[layer]) layers[layer].add(renderable)
    }

    fun unregisterRenderable(renderable: Renderable? = null, entity: Entity? = null) {
        if (entity?.renderable != null) {
            val layer = entity.renderable!!.layer
            if (layers[layer].contains(renderable)) layers[layer].remove(renderable)
            entity.debugHitboxes.forEach { unregisterRenderable(entity = it) }
        }
        if (renderable != null) {
            val layer = renderable.layer
            if (layers[layer].contains(renderable)) layers[layer].remove(renderable)
        }
    }

    private fun clearScreen(canvas: Canvas, context: Context) {
        // Just draw a white rectangle over the entire screen. We might change it to "our"
        // background color. Might not be necessary when we have a background image
        val borderHeight = relY(29f / 64f)
        paint.style = Paint.Style.FILL

        paint.color = ContextCompat.getColor(context, R.color.colorPrimary)// Color.parseColor("#D2EACE")
        canvas.drawRect(0f, 0f, worldWidth, borderHeight, paint)
        paint.color = ContextCompat.getColor(context, R.color.colorAccent) //Color.parseColor("#FFE2A1")
        canvas.drawRect(0f, borderHeight, worldWidth, worldHeight, paint)
    }

    private fun drawRenderable(canvas: Canvas, renderable: Renderable) {
        when (renderable) {
            is Sprite -> {
                drawSprite(renderable, canvas)
            }
            is Circle -> {
                drawCircle(renderable, canvas)
            }
            is Rectangle -> {
                drawRectangle(renderable, canvas)
            }
            is Text -> {
                drawText(renderable, canvas)
            }
        }
    }

    private fun correctedPosition(position: Vector2D, zoomable: Boolean): Vector2D {
        return if (selectedSection == -1 || !zoomable) {
            position
        } else {
            val cx =
                (selectedSection - 1) * (worldWidth / numberOfSections) + (worldWidth / (2f * numberOfSections))
            val cy = 1f * (worldHeight / numberOfSections) + (worldHeight / (2f * numberOfSections))
            Vector2D(cx, cy)
            val dx = position.x - cx
            val dy = position.y - cy
            Vector2D(worldCenter.x + dx * numberOfSections, worldCenter.y + dy * numberOfSections)
        }
    }

    private fun correctedDimensions(dimensions: Dimensions, zoomable: Boolean): Dimensions {
        return if (selectedSection == -1 || !zoomable) dimensions
        else {
            Dimensions(dimensions.width * numberOfSections, dimensions.height * numberOfSections)
        }
    }

    private fun correctedRadius(radius: Float, zoomable: Boolean): Float {
        return if (selectedSection == -1 || !zoomable) radius else radius * numberOfSections.toFloat()
    }

    private fun drawSprite(sprite: Sprite, canvas: Canvas) {
        val correctedPosition = correctedPosition(sprite.position, sprite.zoomable)
        val correctedDimensions = correctedDimensions(sprite.dimensions, sprite.zoomable)
        var centerX = correctedPosition.x - correctedDimensions.width * (1f / 2f)
        var centerY = correctedPosition.y - correctedDimensions.height * (1f / 2f)
        //if (centerX + correctedDimensions.width !in 0f..worldWidth || centerY + correctedDimensions.height !in 0f..worldHeight) { return }

        if (!sprite.drawCentered) {
            centerX += correctedDimensions.width * (1f / 4f)
            centerY += correctedDimensions.height * (1f / 2f)
        }
        val m = Matrix()
        m.setTranslate(centerX, centerY)
        m.postRotate(sprite.rotation, centerX, centerY)
        if (sprite.hasOutline) {
            drawOutline(
                canvas = canvas,
                sprite = sprite,
                cx = centerX,
                cy = centerY,
                strokeWidth = 10f
            )
        }
        val smallBitmap = GameTextures.textures[sprite.textureName]
        val bigBitmap = GameTextures.zoomedTextures[sprite.textureName]
        val bitmap =
            if ((selectedSection == -1 || !sprite.zoomable) && !sprite.bigTexture) smallBitmap
            else bigBitmap
        if (bitmap != null && sprite.cooked == 0f) canvas.drawBitmap(bitmap, m, null)
        else if (bitmap != null) {
            val paint = Paint()
            paint.colorFilter = getCookedColorFilter(sprite.cooked)
            canvas.drawBitmap(bitmap, m, paint)
        } else log("Texture ${sprite.textureName} did not exist!")
    }

    private fun drawOutline(
        canvas: Canvas,
        sprite: Sprite,
        cx: Float,
        cy: Float,
        strokeWidth: Float
    ) {
        if (GameTextures.textures[sprite.textureName] == null) return
        val correctedDimensions = correctedDimensions(sprite.dimensions, sprite.zoomable)
        // Sources for colortransform matrix:
        // https://developer.android.com/reference/android/graphics/ColorMatrix.html
        // https://stackoverflow.com/questions/4354939/understanding-the-use-of-colormatrix-and-colormatrixcolorfilter-to-modify-a-draw
        val colorTransform: FloatArray = floatArrayOf(
            1f, 1f, 1f, 1f, 1f,
            0f, 0f, 0f, 0f, 0f,
            0f, 0f, 0f, 0f, 0f,
            1f, 1f, 1f, 1f, 1f
        )
        // Using a colormatrix filter for making a bitmap red: https://stackoverflow.com/questions/5699810/how-to-change-bitmap-image-color-in-android
        val colorMatrix = ColorMatrix(colorTransform)
        val colorFilter = ColorMatrixColorFilter(colorMatrix)
        val outlinePaint = Paint()
        outlinePaint.colorFilter = colorFilter
        // Drawing another bitmap under a bitmap in order to outline it: https://stackoverflow.com/questions/6186011/how-to-draw-a-stroke-in-a-bitmap-using-canvas
        val outlineHeight = correctedDimensions.height + 2 * strokeWidth
        val outlineWidth = correctedDimensions.width + 2 * strokeWidth
        val outlineX = cx - strokeWidth
        val outlineY = cy - strokeWidth
        val outlineBitmap = Bitmap.createScaledBitmap(
            GameTextures.textures[sprite.textureName]!!,
            outlineWidth.toInt(),
            outlineHeight.toInt(),
            true
        )
        val outlineMatrix = Matrix()
        outlineMatrix.setTranslate(outlineX, outlineY)
        canvas.drawBitmap(outlineBitmap, outlineMatrix, outlinePaint)
    }

    private fun getCookedColorFilter(cooked: Float): ColorMatrixColorFilter {
        val strength = 0.5f - 0.13f * (if (cooked < 3) cooked else 3f)
        val red = strength
        val green = strength / 2f
        val blue = 0f
        val colorTransform: FloatArray = floatArrayOf(
            red, red, red, red, red,
            green, green, green, green, green,
            blue, blue, blue, blue, blue,
            1f, 1f, 1f, 1f, 1f
        )
        val colorMatrix = ColorMatrix(colorTransform)
        return ColorMatrixColorFilter(colorMatrix)
    }

    private fun drawText(text: Text, canvas: Canvas) {
        val correctedPosition = correctedPosition(text.position, text.zoomable)
        if (correctedPosition.x !in 0f..worldWidth || correctedPosition.y !in 0f..worldHeight) return
        paint.color = text.color
        paint.style = text.style
        paint.textSize =
            text.size * if (selectedSection == -1 || !text.zoomable) 1f else numberOfSections.toFloat()
        paint.strokeWidth = text.strokeWidth
        paint.textAlign = text.align
        canvas.drawText(text.value, correctedPosition.x, correctedPosition.y, paint)
    }

    private fun drawCircle(circle: Circle, canvas: Canvas) {
        val correctedPosition = correctedPosition(circle.position, circle.zoomable)
        val correctedRadius = correctedRadius(circle.radius, circle.zoomable)
        val centerX = correctedPosition.x
        val centerY = correctedPosition.y
        paint.color = circle.color
        paint.style = circle.style
        paint.strokeWidth = 1f
        canvas.drawCircle(centerX, centerY, correctedRadius, paint)
    }

    private fun drawRectangle(rectangle: Rectangle, canvas: Canvas) {
        val correctedPosition = correctedPosition(rectangle.position, rectangle.zoomable)
        val correctedDimensions = correctedDimensions(rectangle.dimensions, rectangle.zoomable)
        val left = correctedPosition.x
        val right = correctedPosition.x + correctedDimensions.width
        val top = correctedPosition.y
        val bottom = correctedPosition.y + correctedDimensions.height
        //if ((left !in 0f..worldWidth && right !in 0f..worldWidth) || (top !in 0f..worldHeight && bottom !in 0f..worldHeight)) return
        paint.color = rectangle.color
        paint.style = rectangle.style
        paint.strokeWidth = 1f
        canvas.drawRoundRect(
            left,
            top,
            right,
            bottom,
            rectangle.rounding,
            rectangle.rounding,
            paint
        )
    }
}