package com.wok_in_progress.rendering

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.DisplayMetrics
import com.wok_in_progress.R
import com.wok_in_progress.log
import com.wok_in_progress.error

object GameTextures {
    var loading = true
    val textures: MutableMap<String, Bitmap> = mutableMapOf()
    val zoomedTextures: MutableMap<String, Bitmap> = mutableMapOf()
    private val nonScaling: MutableList<String> = mutableListOf()
    private val options = BitmapFactory.Options()

    fun loadTextures(context: Context) {
        loading = true
        val res = context.resources
        options.inDensity = DisplayMetrics.DENSITY_XXXHIGH

        //Tomato
        var id = R.drawable.tomato
        createTexture(res, id, "tomato", TextureSizes.tomatoSize)

        id = R.drawable.tomato_sliced
        createTexture(res, id, "tomato_sliced", TextureSizes.tomatoSize)

        id = R.drawable.tomato_sliced_cooked
        createTexture(res, id, "tomato_sliced_cooked", TextureSizes.tomatoSize)

        //Fridge
        id = R.drawable.fridge
        createTexture(res, id, "fridge", TextureSizes.fridgeSize, TextureSizes.fridgeHeight)

        //Deliverytable
        id = R.drawable.presentation_table
        createTexture(
            res,
            id,
            "deliveryTable",
            TextureSizes.deliveryTableSize,
            TextureSizes.deliveryTableHeight
        )

        //Cutting section
        id = R.drawable.cutting_board
        createTexture(
            res,
            id,
            "cuttingSection",
            TextureSizes.cuttingSectionSize,
            TextureSizes.cuttingSectionHeight
        )

        //Oven
        id = R.drawable.oven
        createTexture(res, id, "oven", TextureSizes.ovenSize, TextureSizes.ovenHeight)

        //Deliveryplate
        id = R.drawable.plate
        createTexture(res, id, "deliveryplate", TextureSizes.plateSize, TextureSizes.plateHeight)

        //Fridge open
        id = R.drawable.fridge_open
        createTexture(res, id, "fridge_open", TextureSizes.fridgeSize, TextureSizes.fridgeHeight)

        //Oven
        id = R.drawable.oven
        createTexture(res, id, "oven", TextureSizes.ovenSize, TextureSizes.ovenHeight)

        //Plate
        id = R.drawable.plate
        createTexture(res, id, "plate", TextureSizes.plateSize, TextureSizes.plateHeight)

        //Knife
        id = R.drawable.knife
        createTexture(res, id, "knife", TextureSizes.knifeSize, TextureSizes.knifeHeight)

        //Pan
        id = R.drawable.pan
        createTexture(res, id, "pan", TextureSizes.panSize, TextureSizes.panHeight)

        //Egg
        id = R.drawable.egg
        createTexture(res, id, "egg", TextureSizes.eggSize, TextureSizes.eggHeight)

        //Steak
        id = R.drawable.steak
        createTexture(res, id, "steak", TextureSizes.steakSize, TextureSizes.steakHeight)

        //Patty
        id = R.drawable.hamburger_patty
        createTexture(res, id, "patty", TextureSizes.pattySize, TextureSizes.pattyHeight)

        //Patty_cooked
        id = R.drawable.hamburger_patty_cooked
        createTexture(res, id, "patty_cooked", TextureSizes.pattySize, TextureSizes.pattyHeight)

        //HamburgerBunTop
        id = R.drawable.hamburger_bread_flipped
        createTexture(
            res,
            id,
            "hamburgerBunTop",
            TextureSizes.hamburgerBunTopSize,
            TextureSizes.hamburgerBunTopHeight
        )

        //HamburgerBunBottom
        id = R.drawable.hamburger_bread
        createTexture(
            res,
            id,
            "hamburgerBunBottom",
            TextureSizes.hamburgerBunBottomSize,
            TextureSizes.hamburgerBunBottomHeight
        )

        //Cheese
        id = R.drawable.cheese
        createTexture(res, id, "cheese", TextureSizes.cheeseSize, TextureSizes.cheeseHeight)

        //lettuceCut
        id = R.drawable.lettuce
        createTexture(res, id, "lettuce", TextureSizes.lettuceSize, TextureSizes.lettuceHeight)

        //bacon
        id = R.drawable.bacon
        createTexture(res, id, "bacon", TextureSizes.baconSize, TextureSizes.baconHeight)

        //Cooked bacon
        id = R.drawable.bacon_cooked
        createTexture(res, id, "bacon_cooked", TextureSizes.baconSize, TextureSizes.baconHeight)
        //Load the rest by setting id before using createTexture

        // Creating the smaller textures for the zoomed out versions
        createScaledTextures(1f / Renderer.numberOfSections.toFloat())
        loading = false
    }

    fun createScaledTextures(scalar: Float) {
        for (key in zoomedTextures.keys) {
            if (key !in nonScaling && zoomedTextures[key] != null) {
                val newTexture = createTextureFromBitmap(
                    key,
                    zoomedTextures[key]!!.width.toFloat() * scalar,
                    zoomedTextures[key]!!.height.toFloat() * scalar
                )
                if (newTexture != null) {
                    textures[key] = newTexture
                }
            }
        }
    }

    private fun createTexture(
        res: Resources,
        id: Int,
        textureName: String,
        width: Float,
        height: Float = width
    ) {
        val decodedTexture = BitmapFactory.decodeResource(res, id, options)
        if (decodedTexture == null) error("$textureName texture was not found")
        else {
            zoomedTextures[textureName] = decodedTexture
            changeDimensions(textureName, width, height)
        }
    }

    private fun createTextureFromBitmap(textureName: String, width: Float, height: Float): Bitmap? {
        return if (zoomedTextures[textureName] != null) {
            Bitmap.createScaledBitmap(
                zoomedTextures[textureName]!!,
                width.toInt(),
                height.toInt(),
                false
            )
        } else null
    }

    /**
     * Only used when creating initial larger textures, therefore the upscaling scaledSize
     */
    private fun changeDimensions(textureName: String, width: Float, height: Float = width) {
        if (zoomedTextures[textureName] != null) {
            val scaledWidth = width * Renderer.numberOfSections
            val scaledHeight = height * Renderer.numberOfSections
            zoomedTextures[textureName] = Bitmap.createScaledBitmap(
                zoomedTextures[textureName]!!,
                scaledWidth.toInt(),
                scaledHeight.toInt(),
                false
            )
        }
    }


}