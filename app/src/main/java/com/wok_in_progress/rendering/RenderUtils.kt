package com.wok_in_progress.rendering

/**
 * @param x A percentage of the screen width
 */
fun relX(x: Float): Float {
    return Renderer.worldWidth * x
}

/**
 * @param y A percentage of the screen height
 */
fun relY(y: Float): Float {
    return Renderer.worldHeight * y
}