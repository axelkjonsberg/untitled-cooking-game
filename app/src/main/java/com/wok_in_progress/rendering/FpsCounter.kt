package com.wok_in_progress.rendering

import com.wok_in_progress.entities.EntityManager
import com.wok_in_progress.log


object FpsCounter {


    private var frameCounter: Int = 1
    private var lastTime = System.currentTimeMillis() - 1
    private var lastFps = 0
    private var average = 0f
    private var totalTime = 0f
    private var counts = 0

    fun incrementCounter() {
        // Get the current time
        val currentTime = System.currentTimeMillis()

        // When a second has passed, calculate the framerate and reset
        lastTime = if (currentTime - lastTime > 1000) {
            lastFps = (frameCounter * 1000 / (currentTime - lastTime)).toInt()
            totalTime += lastFps
            counts++
            average = (totalTime / counts)
            if (counts % 5 == 0) log("FpsCounter: Average fps: $average, current fps: $lastFps")
            frameCounter = 1
            currentTime
        } else {
            frameCounter++
            lastTime
        }
    }

}