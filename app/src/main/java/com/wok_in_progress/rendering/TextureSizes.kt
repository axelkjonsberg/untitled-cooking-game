package com.wok_in_progress.rendering

// For performance reasons it is easiest to define base sizes here and use them
// in GameTextures and the entities (Tomato for now)
object TextureSizes {
    var deliveryTableSize: Float = 0f
    var deliveryTableHeight: Float = 0f

    var cuttingSectionSize: Float = 0f
    var cuttingSectionHeight: Float = 0f

    var fridgeSize: Float = 100f
    var fridgeHeight: Float = fridgeSize / (600f / 551f)

    var ovenSize: Float = 0f
    var ovenHeight: Float = 0f

    var plateSize: Float = 162f
    var plateHeight: Float = plateSize / (426f / 99f)
    var bigPlateSize: Float = 0f

    var knifeSize: Float = 100f
    var knifeHeight: Float = knifeSize / (874f / 89f)

    var panSize: Float = 100f
    var panHeight: Float = panSize / (1071f / 166f)

    // Ingredients

    var baconSize: Float = 40f
    var baconHeight: Float = baconSize / (286f / 50f)

    var cheeseSize: Float = 40f
    var cheeseHeight: Float = cheeseSize / (329f / 43f)

    var tomatoSize: Float = 30f
    var tomatoHeight: Float = tomatoSize / (216f / 210f)

    var pattySize: Float = 40f
    var pattyHeight: Float = pattySize / (374f/120f)

    var tomatoSlicedSize: Float = 30f
    var tomatoSlicedHeight: Float = tomatoSlicedSize / (230f/102f)

    var eggSize: Float = 25f
    var eggHeight: Float = eggSize / (148f / 197f)

    var steakSize: Float = 40f
    var steakHeight: Float = 40f

    var hamburgerBunTopSize: Float = 50f
    var hamburgerBunTopHeight: Float = hamburgerBunTopSize / (473f / 164f)

    var hamburgerBunBottomSize: Float = 50f
    var hamburgerBunBottomHeight: Float = hamburgerBunTopSize / (473f / 164f)

    var lettuceSize: Float = 50f
    var lettuceHeight: Float = lettuceSize / (375f / 60f)

    fun setScreenDependentSizes(screenWidth: Float, screenHeight: Float) {
        // Layout
        deliveryTableSize = screenWidth / 4f
        deliveryTableHeight = deliveryTableSize / (1440f / 2560f)
        cuttingSectionSize = screenWidth / 4f
        cuttingSectionHeight = cuttingSectionSize / (1440f / 2560f)
        plateSize = screenWidth * (3f / 16f)
        plateHeight = plateSize / (426f / 99f)
        ovenSize = screenWidth / 4f
        ovenHeight = ovenSize / (1440f / 2560f)
        bigPlateSize = screenWidth
        fridgeSize = (screenWidth / 4f) * 0.8f
        fridgeHeight = fridgeSize / (1104f / 2368f)
        panSize = screenWidth * (1f / 8f)
        panHeight = panSize / (1071f / 166f)
        // Ingredients

    }
}