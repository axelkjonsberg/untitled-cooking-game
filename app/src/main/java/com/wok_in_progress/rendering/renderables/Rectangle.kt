package com.wok_in_progress.rendering.renderables

import android.graphics.Paint
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.RectangleHitbox
import com.wok_in_progress.physics.Vector2D

class Rectangle(
    override var color: Int,
    override var style: Paint.Style,
    val rectangleHitbox: RectangleHitbox? = null,
    override val zoomable: Boolean = true,
    override var position: Vector2D = rectangleHitbox?.position ?: Vector2D(),
    override var dimensions: Dimensions = Dimensions(rectangleHitbox?.dimensions ?: Dimensions(0f)),
    override val layer: Int = 3,
    override var rotation: Float = rectangleHitbox?.rotation ?: 0f,
    override val drawCentered: Boolean = false,
    val rounding: Float = 0f
) : Shape {

    constructor(other: Rectangle) : this(
        color = other.color,
        style = other.style,
        rectangleHitbox = other.rectangleHitbox,
        zoomable = other.zoomable,
        layer = other.layer,
        drawCentered = other.drawCentered,
        rounding = other.rounding
    )
}