package com.wok_in_progress.rendering.renderables

import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.Vector2D


class Sprite(
    override var position: Vector2D,
    override var dimensions: Dimensions,
    override val layer: Int,
    override var rotation: Float = 0f,
    override var drawCentered: Boolean = true,
    override val zoomable: Boolean = true,
    val bigTexture: Boolean = false,
    var textureName: String,
    var cooked: Float = 0f,
    var hasOutline: Boolean = false
) : Renderable {

    constructor(other: Sprite) : this(
        position = other.position.copy(),
        dimensions = Dimensions(other.dimensions),
        layer = other.layer,
        rotation = other.rotation,
        drawCentered = other.drawCentered,
        zoomable = other.zoomable,
        bigTexture = other.bigTexture,
        textureName = other.textureName,
        cooked = other.cooked
    )
}