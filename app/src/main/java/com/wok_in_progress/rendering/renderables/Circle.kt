package com.wok_in_progress.rendering.renderables

import android.graphics.Paint
import com.wok_in_progress.physics.CircleHitbox
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.Vector2D

class Circle(
    override var color: Int,
    override var style: Paint.Style,
    val circleHitbox: CircleHitbox,
    override val zoomable: Boolean = true,
    override var position: Vector2D = circleHitbox.position,
    override var dimensions: Dimensions = Dimensions(circleHitbox.radius, circleHitbox.radius),
    var radius: Float = circleHitbox.radius,
    override val layer: Int = 3,
    override var rotation: Float = circleHitbox.rotation,
    override val drawCentered: Boolean = false
) : Shape {

    constructor(other: Circle) : this(
        color = other.color,
        style = other.style,
        circleHitbox = other.circleHitbox,
        zoomable = other.zoomable,
        layer = other.layer,
        drawCentered = other.drawCentered
    )
}