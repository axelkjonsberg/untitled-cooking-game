package com.wok_in_progress.rendering.renderables

import android.graphics.Color
import android.graphics.Paint
import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.Vector2D

class Text(
    override var position: Vector2D,
    override var dimensions: Dimensions = Dimensions(0f, 0f),
    override val layer: Int = 3,
    override var rotation: Float = 0f,
    override val drawCentered: Boolean = false,
    override val zoomable: Boolean = true,
    var value: String,
    var color: Int = Color.BLACK,
    var size: Float = 50f,
    var strokeWidth: Float = 1f,
    var style: Paint.Style = Paint.Style.FILL_AND_STROKE,
    var align: Paint.Align,
    var centeredVertical: Boolean = false // Or start or end
) : Renderable {

    constructor(other: Text) : this(
        position = other.position.copy(),
        layer = other.layer,
        rotation = other.rotation,
        drawCentered = other.drawCentered,
        zoomable = other.zoomable,
        value = other.value,
        color = other.color,
        size = other.size,
        strokeWidth = other.strokeWidth,
        style = other.style,
        align = other.align,
        centeredVertical = other.centeredVertical
    )
}