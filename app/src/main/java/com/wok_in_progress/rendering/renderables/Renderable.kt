package com.wok_in_progress.rendering.renderables

import com.wok_in_progress.physics.Dimensions
import com.wok_in_progress.physics.Vector2D

interface Renderable {
    var position: Vector2D
    var dimensions: Dimensions
    var rotation: Float
    val layer: Int
    val drawCentered: Boolean
    val zoomable: Boolean

    fun updatePosition(newPosition: Vector2D) {
        position = Vector2D(newPosition)
    }

}


