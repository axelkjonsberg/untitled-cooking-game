package com.wok_in_progress.rendering.renderables

import android.graphics.Paint

interface Shape : Renderable {
    var color: Int
    var style: Paint.Style
}