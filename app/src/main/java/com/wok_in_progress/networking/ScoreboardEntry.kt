package com.wok_in_progress.networking

import com.google.firebase.Timestamp

/**
 * A data class to represent entries of the "scores" collection in firestore.
 * @property name Name of the score holder - String
 * @property score Their final score - Int
 * @property date Date and time of day the score was set - FireBase Timestamp
 * @property versionNumber Version number the score was set - FireBase Number
 */
data class ScoreboardEntry(
    val name: String? = null,
    val score: Int? = null,
    val date: Timestamp? = null,
    // How the score is calculated might change in the future, so we should keep track of on which
    // version number of the game the score was set.
    val versionNumber: Float? = null
)