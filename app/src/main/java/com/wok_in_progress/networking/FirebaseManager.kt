package com.wok_in_progress.networking

import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await

object FirebaseManager {
    private val db = Firebase.firestore

    /**
     * Add a document to a specified collection of the database.
     * If the collection does not allready exists, it will be created automatically.
     * @param doc instance of a DataClass representing the document to be written in the database
     * @param collectionPath The path url of the desired collection.
     */
    internal fun createDocument(doc: ScoreboardEntry, collectionPath: String){
        db.collection(collectionPath).add(doc)
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }
    }

    internal fun getDocument(documentPath: String, collectionPath: String){
        val docRef = db.collection(collectionPath).document(documentPath)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                } else {
                    Log.d(TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
            }
    }

    /**
     * @param querySize Defaults to a size of 5.
     * @return List of DocumentSnapshot from "scoreboard"
     */
    suspend fun getTopScoreHolders(querySize: Long = 5) : MutableList<ScoreboardEntry> {
        val topScoreHolders = mutableListOf<ScoreboardEntry>()
        val snapshot = db.collection("scoreboard")
            .orderBy("score", Query.Direction.DESCENDING)
            .limit(querySize)
            .get()
            .await()

        for (document in snapshot.documents) {
            document.toObject(ScoreboardEntry::class.java)?.let { topScoreHolders.add(it) }
        }
        return topScoreHolders
    }

    fun writeScore(scoreboardEntry: ScoreboardEntry) {
        createDocument(
            scoreboardEntry,
            "scoreboard"
        )
    }
}