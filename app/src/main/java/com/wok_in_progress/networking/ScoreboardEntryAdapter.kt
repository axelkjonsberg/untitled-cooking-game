package com.wok_in_progress.networking

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.wok_in_progress.R


class ScoreboardEntryAdapter(items: MutableList<ScoreboardEntry>, ctx: Context) :
    ArrayAdapter<ScoreboardEntry>(ctx, R.layout.list_item_scoreboard, items) {

    private class ScoreboardViewHolder {
        internal var name: TextView? = null
        internal var score: TextView? = null
        internal var number: TextView? = null
        // internal var date: TextView? = null
    }

    override fun getView(i: Int, view: View?, viewgroup: ViewGroup): View {
        var view = view

        val viewHolder: ScoreboardViewHolder

        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.list_item_scoreboard, viewgroup, false)

            viewHolder = ScoreboardViewHolder()
            viewHolder.name = view!!.findViewById<View>(R.id.scoreboardentry_name) as TextView
            viewHolder.score = view.findViewById<View>(R.id.scoreboardentry_score) as TextView
            viewHolder.number = view.findViewById<View>(R.id.scoreboardentry_number) as TextView
            // viewHolder.date = view.findViewById<View>(R.id.scoreboardentry_date) as TextView
        } else {
            //no need to call findViewById, can use existing ones from saved view holder
            viewHolder = view.tag as ScoreboardViewHolder
        }

        val scoreboardEntry = getItem(i)
        viewHolder.name!!.text = scoreboardEntry!!.name
        viewHolder.score!!.text = scoreboardEntry.score.toString()
        viewHolder.number?.text = (i + 1).toString()
        // viewHolder.date!!.text = scoreboardEntry.Date

        view.tag = viewHolder

        return view
    }
}