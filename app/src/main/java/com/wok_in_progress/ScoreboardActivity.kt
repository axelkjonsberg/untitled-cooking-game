package com.wok_in_progress

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.Timestamp
import com.wok_in_progress.networking.FirebaseManager
import com.wok_in_progress.networking.ScoreboardEntry
import com.wok_in_progress.networking.ScoreboardEntryAdapter
import com.wok_in_progress.rules.Game
import kotlinx.android.synthetic.main.activity_scoreboard.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


class ScoreboardActivity : AppCompatActivity() {

    lateinit var adapter: ScoreboardEntryAdapter
    lateinit var scoreboardView: ListView
    lateinit var playerScoreView: TextView
    lateinit var playerNameEditText: EditText
    private val topScoreHolders: MutableList<ScoreboardEntry> = mutableListOf()
    private var scoreSubmitted: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scoreboard)

        scoreSubmitted = false

        val submitButton: Button = findViewById(R.id.submitButton)
        val mainMenuButton: Button = findViewById(R.id.mainMenuButton)
        val retryButton: Button = findViewById(R.id.retryButton)
        playerNameEditText = findViewById(R.id.playerName)
        playerScoreView = findViewById(R.id.playerScore)

        val score: Int?
        score = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getInt("SCORE")
        } else {
            savedInstanceState.getSerializable("SCORE") as Int?
        }

        if (score != null) {
            playerScoreView.text = "Your score: $score"
            playerNameEditText.setOnKeyListener(object : View.OnKeyListener {
                override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                    // If the event is a key-down event on the "enter" button
                    if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                        submitScore(score, playerNameEditText.text.toString())
                        return true
                    }
                    return false
                }
            })
            submitButton.setOnClickListener {
                submitScore(score, playerNameEditText.text.toString())
            }
        } else {
            log("No score")
            // Hide everything if there is no score (we are not coming from a finished game)
            playerScoreView.visibility = View.GONE
            playerNameEditText.visibility = View.INVISIBLE
            playerNameEditText.isEnabled = false
            playerNameEditText
            retryButton.isEnabled = false
            retryButton.visibility = View.INVISIBLE
            submitButton.isEnabled = false
            submitButton.visibility = View.INVISIBLE
        }

        adapter = ScoreboardEntryAdapter(topScoreHolders, this)
        scoreboardView = findViewById(R.id.scoreboard)
        scoreboardView.adapter = adapter
        loadData()
    }

    private fun submitScore(score: Int, name: String) {
        if (!scoreSubmitted) {
            if (stringHasContent(name)) {
                val date = Timestamp.now()
                val scoreboardEntry = ScoreboardEntry(name, score, date, versionNumber)
                FirebaseManager.writeScore(scoreboardEntry)
                scoreSubmitted = true
                Toast.makeText(this, "Your score was submitted", Toast.LENGTH_LONG).show()
                playerNameEditText.text.clear()
                submitButton.isEnabled = false
                loadData()
            }
        }
    }

    private fun loadData() = CoroutineScope(Dispatchers.Main).launch {
        val task = async(Dispatchers.IO) {
            FirebaseManager.getTopScoreHolders()
        }
        topScoreHolders.clear()
        topScoreHolders.addAll(task.await())
        adapter.notifyDataSetChanged()
    }

    fun goToMain(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun goToGame(view: View) {
        val intent = Intent(this, GameActivity::class.java)
        Game.reset()
        startActivity(intent)
    }
}