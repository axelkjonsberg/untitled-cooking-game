package com.wok_in_progress

import com.wok_in_progress.physics.*
import org.junit.Test

class PhysicsTests {
    @Test
    fun rectanglesCollision_IsCorrect() {
        val rectangleA = RectangleHitbox(position = Vector2D(0f, 0f), dimensions = Dimensions(50f, 50f), dx = 0f, dy = 0f)
        val rectangleB = RectangleHitbox(position = Vector2D(40f, 0f), dimensions = Dimensions(50f, 50f), dx = 0f, dy = 0f)
        assert(isOverlapRectangles(rectangleA, rectangleB))
        rectangleB.updatePosition(50f, 0f)
        assert(isOverlapRectangles(rectangleA, rectangleB))
        rectangleB.updatePosition(50.001f, 0f)
        assert(!isOverlapRectangles(rectangleA, rectangleB))
        rectangleB.updatePosition(0f, 49.99f)
        assert(isOverlapRectangles(rectangleA, rectangleB))
        rectangleB.updatePosition(-50f, 0f)
        assert(isOverlapRectangles(rectangleA, rectangleB))
        rectangleB.updatePosition(-50f, -50f)
        assert(isOverlapRectangles(rectangleA, rectangleB))
        rectangleB.updatePosition(50f, 50f)
        rectangleA.updatePosition(100f, 100f)
        assert(isOverlapRectangles(rectangleA, rectangleB))
    }

    @Test
    fun isOverlapHitboxesLine_IsCorrect() {
        val rect = RectangleHitbox(position = Vector2D(), dimensions = Dimensions(100f, 10f), dx = 0f, dy = 0f)
        val body = PhysicalBody(position = Vector2D(0f, 0f), dimensions = Dimensions(100f, 10f), rectangleHitboxes = listOf(rect))
        val line = Line(Vector2D(x = 0f, y = 2f), Vector2D(x = 101f, y = 2f))
        assert(isOverlapHitboxesLine(body, line))
    }

    @Test
    fun doesLineIntersectRectangle_IsCorrect() {
        val rect = RectangleHitbox(position = Vector2D(), dimensions = Dimensions(100f, 10f), dx = 0f, dy = 0f)
        var line = Line(Vector2D(x = -5f, y = 2f), Vector2D(x = 101f, y = 2f))
        assert(doesLineIntersectRectangle(line, rect))
        line = Line(Vector2D(x = 0f, y = -2f), Vector2D(x = 101f, y = -2f))
        assert(!(doesLineIntersectRectangle(line, rect)))
    }

    @Test
    fun doesLineIntersectCircle_IsCorrect() {
        val circle = CircleHitbox(position = Vector2D(), radius = 10f, dx = 0f, dy = 0f)
        var line = Line(Vector2D(-10f, 0f), Vector2D(10f, 0f))
        assert(doesLineIntersectCircle(line, circle))
        line = Line(Vector2D(0f, 11f), Vector2D(20f, 11f))
        assert(!doesLineIntersectCircle(line, circle))
    }

    @Test
    fun doesLineIntersectLine_IsCorrect() {
        val a = Line(Vector2D(0f, 10f), Vector2D(10f, 10f))
        var b = Line(Vector2D(5f, 5f), Vector2D(5f, 15f))
        assert(doesLineIntersectLine(a, b))
        b = Line(Vector2D(0f, 9f), Vector2D(10f, 11f))
        assert(doesLineIntersectLine(a, b))
        b = Line(Vector2D(0f, 11f), Vector2D(10f, 11f))
        assert(!doesLineIntersectLine(a, b))
    }
}