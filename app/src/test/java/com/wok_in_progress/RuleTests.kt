package com.wok_in_progress

import com.wok_in_progress.entities.Ingredient
import com.wok_in_progress.entities.ingredient_entities.Tomato
import com.wok_in_progress.physics.Vector2D
import com.wok_in_progress.rules.RuleManager
import org.junit.Test

class RuleTests {
    @Test
    fun getScore_IsCorrect() {
        val tomato1 =
            Tomato(
                position = Vector2D(50f, 50f),
                textureName = "tomato"
            )
        tomato1.ruleBody?.isPrepared = true
        tomato1.ruleBody?.cooked = 2f

        val deliveredDish: List<Ingredient> = listOf(tomato1)
        val fetched: List<Ingredient> = listOf(tomato1)
        RuleManager.setInput(deliveredDish, fetched)
        val score: Int = RuleManager.getScore()

        println(score)

    }
}